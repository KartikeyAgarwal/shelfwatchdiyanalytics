from flask import Flask, json, request, jsonify, make_response
from flask_restful import Resource, Api
from flask_cors import CORS, cross_origin
# import redis

# redis_cache = redis.Redis(host='localhost',port=6379, db=0)

app = Flask(__name__)
api = Api(app)
cors = CORS(app)

jsonV = {"key1": "value1", "key2": "value2"}
# redis_cache.set("name1","Kartikey")
# x = redis_cache.get("name")
# print(x)
# print(redis_cache.get("name1"))

app.config['CORS_HEADERS'] = 'Content-Type'


class Main(Resource):
    @cross_origin()
    def get(self):
        return jsonify(jsonV)

    def post(self):
        if request.is_json:
            req = request.get_json()
            print(req)
            return jsonify(req)
        else:
            return "request was not json, POST request"


api.add_resource(Main, '/')
app.run(debug=True)