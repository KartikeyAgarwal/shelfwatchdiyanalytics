import couchbase
from couchbase.cluster import Cluster, ClusterOptions
from couchbase.cluster import PasswordAuthenticator

# conn_url="http://localhost:8091/"
conn_url = "couchbase://localhost"
username="Admin"
cb_password="shelfwatch"

# def couchbasedb_cluster():
cluster = Cluster(conn_url, ClusterOptions(PasswordAuthenticator(username, cb_password)))
cb=cluster.bucket('DIY_Analytics')
    # return cluster

cb_collection = cb.default_collection()

doc={
    "id":45,
    "name":"analytics",
    "company":"Paralleldots"
}

def upload_data(doc):
    key=str(doc["name"])+"_"+str(doc["id"])
    result = cb_collection.upsert(key,doc)
    print(result.cas)

upload_data(doc)