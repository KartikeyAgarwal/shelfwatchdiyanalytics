import re, json
import uvicorn, itertools
from pymysql import cursors
from db import db_connection
from fastapi import FastAPI, APIRouter, Request
from datetime import date, datetime
import couchbase
from couchbase.cluster import Cluster, ClusterOptions, QueryOptions
from couchbase.cluster import PasswordAuthenticator

conn_url = "couchbase://localhost"
username="Admin"
cb_password="shelfwatch"

cluster = Cluster(conn_url, ClusterOptions(PasswordAuthenticator(username, cb_password)))
cb=cluster.bucket('DIY_Analytics')
cb_collection = cb.default_collection()

app=FastAPI()

user_id = '4da91f3a-600d-4598-9e22-0ed34c35393b'
role_id = '4ef000b2-c2e7-4622-a9c7-947642d6d5c9'


def get_analytics_table_data(user_id):
    conn=db_connection()
    cursor=conn.cursor()
    # d = "2020-02-09 12:02:15"
    # d= datetime.strptime(d,'%Y-%m-%d %H:%M:%S')
    # sql = "INSERT INTO shelfwatch_dev.diy_analytics(id,analytics_name,status,user_id,project_id,td_version_id,created_at,modified_at,delete_status) \
    # VALUES(1,'first_analytics',1,1,1,1,%s,%s,0)"
    # cursor.execute(sql,(d,d))
    sql = "SELECT * FROM shelfwatch_dev.diy_analytics as diy WHERE diy.user_id=%s AND diy.delete_status=0"
    cursor.execute(sql,user_id)
    analytics_data = cursor.fetchall()
    print(analytics_data)
    return analytics_data


def validate_analytics_name_exists(analytics_name):
    conn=db_connection()
    cursor=conn.cursor
    sql = "SELECT analytics_name FROM shelfwatch_dev.diy_analytics as diy WHERE diy.analytics_name=%s AND diy.delete_status=0"
    cursor.execute(sql,analytics_name)
    output=cursor.fetchone()
    if output!=None:
        return False
    else:
        return True


def validate_project_td_version(project_id, td_version_id, user_id):
    conn=db_connection()
    cursor=conn.cursor
    sql = "SELECT project_id, td_version_id FROM shelfwatch_dev.diy_analytcis as diy WHERE diy.project_id=%s AND td_version_id=%s \
            AND diy.user_id=%s AND diy.delete_status=0"
    cursor.execute(sql,(project_id,td_version_id,user_id))
    output=cursor.fetchone()
    if output!=None:
        return False
    else:
        return True


def add_to_analytics_table(data):
    name=data["analytics_name"]
    user_id=data["user_id"]
    project_id=data["project_id"]
    td_version_id=data["td_version_id"]
    date_time = datetime.now()
    
    conn = db_connection()
    cursor = conn.cursor()
    
    try:
        sql = "INSERT INTO shelfwatch_dev.diy_analytics(analytics_name,status,user_id,project_id,td_version_id,created_at,modified_at,delete_status) \
        VALUES(%s,%s,%s,%s,%s,%s,%s,%s)"
        cursor.execute(sql, (name,"1",user_id,project_id,td_version_id,date_time,date_time, 0))
        return True
    except:
        return False

def add_analytics_config(data):
    analytics_id = str(data["analytics_id"])
    project_id = str(data["project_id"])
    td_version_id = str(data["td_version_id"])
    config_type = str(data["config_type"])
    key = "_".join([analytics_id,project_id,td_version_id,config_type])
    try:
        result = cb_collection.upsert(key,data)
        return True
    except:
        return False

def delete_analytics_from_table(data):
    try:
        analytics_id=data["id"]
        project_id=data["project_id"]
        td_version_id=data["td_version_id"]
        conn = db_connection()
        cursor = conn.cursor()
        sql = "UPDATE shelfwatch_dev.diy_analytics as diy SET diy.delete_status=1 WHERE diy.id=%s AND diy.project_id=%s AND diy.td_version_id=%s"
        cursor.execute(sql,(analytics_id,project_id,td_version_id))
        return 1
    except:
        return 0


def delete_analytics_from_couchbase(data):
    try:
        analytics_id=data["analytics_id"]
        project_id=data["project_id"]
        td_version_id=data["td_version_id"]

        sql_query = 'DELETE FROM `DIY_Analytics` WHERE analytics_id=$1 AND project_id=$2 AND td_version_id=$3'
        row_itr = cluster.query(sql_query,QueryOptions(positional_parameters=[analytics_id,project_id,td_version_id]))
        # print("deleted")
        return True
    except:
        print("Error occurred while deleting from the couchbase")
        return False


def get_projects(user_id, role_id):
    conn = db_connection()
    cursor = conn.cursor()

    sql = "SELECT u.id as user_pid, r.id as role_pid FROM sw_users as u, sw_user_roles as ur, sw_roles as r WHERE u.id=ur.user_id AND \
    r.id=ur.role_id AND u.user_id=%s AND r.role_id=%s"

    cursor.execute(sql, (user_id, role_id))
    data_u = cursor.fetchone()
    try:
        user_pid = data_u.get('user_pid')
        role_pid = data_u.get('role_pid')
    except:
        return "User doesn't exist"
        
    sql = """
            SELECT 
                DISTINCT p.id, p.project_name,p.created_at, p.project_id, stv.version_name as td_version_name, stv.id as td_version_id 
            FROM 
                shelfwatch_dev.sw_projects as p, shelfwatch_dev.sw_td_versions as stv, 
                shelfwatch_dev.sw_users as u, shelfwatch_dev.sw_user_roles as ur, shelfwatch_dev.sw_roles 
            WHERE stv.project_id=p.id AND ur.role_id=%s AND ur.user_id=%s 
                AND p.id=ur.project_id AND p.delete_status=0 ORDER BY p.id, stv.created_at
        """

    cursor.execute(sql, (role_pid, user_pid))

    projects_data = cursor.fetchall()
    # print(projects_data)
    projects_list = []

    project_data = {"project_pid": "", "project_name": "",
                    "project_id": "", "version_info":[]}

    for key, value in itertools.groupby(projects_data, lambda x: x["id"]):
        project_data["project_pid"] = key
        value_list = [i for i in value]

        if value_list:
            project_data["project_name"] = value_list[0].get("project_name")
            project_data["project_id"] = value_list[0].get("project_id")

            td_version = []
            for i in value_list:
                project_data.update({"version_info":[]})
                td_version.append({"td_version_name": i.get("td_version_name"), "td_version_id":i.get("td_version_id")})
                project_data["version_info"] = td_version

        projects_list.append(project_data)

        project_data = {"project_pid": "", "project_name": "",
                        "project_id": "", "td_version_id": ""}

    cursor.close()
    conn.close()
    # print(projects_list)
    return projects_list



def get_filters(project_pid, td_version_id):

    user_pid = 1
    role_pid = 2

    conn = db_connection()
    cursor = conn.cursor()
    sql = "SELECT pipe.pipeline_name FROM shelfwatch_dev.sw_pipelines as pipe WHERE pipe.pipeline_name!='STREETSNAP'"
    cursor.execute(sql)
    pipe = cursor.fetchall()
    pipe_name_list = []
    for i in pipe:
        pipe_name_list.append(i.get("pipeline_name"))
    # print(pipe_name_list)

    sql = "SELECT DISTINCT pipe.pipeline_name, CASE WHEN pipe.pipeline_name='STREETSNAP' THEN JSON_OBJECT('plano_flag', p.plano_flag, 'sku_flag', p.sku_flag, 'pop_flag', p.pop_flag, 'asset_flag', p.asset_flag) ELSE \
        JSON_OBJECT(pipe.pipeline_name,NULL) END as Pipeline FROM shelfwatch_dev.sw_projects as p, shelfwatch_dev.sw_pipelines as pipe, shelfwatch_dev.sw_td_versions as stv WHERE p.id=%s AND pipe.id=p.pipeline_id \
        AND stv.project_id=p.id"

    cursor.execute(sql, (project_pid))
    pipelines = cursor.fetchall()

    filters_list = {"Pipeline": {}, "Shop Master": {},
                    "Shelf Section": {}, "Attributes": {}}

    for i in range(len(pipelines)):
        pipelines[i]["Pipeline"] = json.loads(pipelines[i]["Pipeline"])
        if(pipelines[i]["pipeline_name"] == 'STREETSNAP'):
            pipe_sub = pipelines[i]["Pipeline"]
            if pipe_sub["pop_flag"] == 1:
                filters_list["Pipeline"]["POP"] = []
            if pipe_sub["sku_flag"] == 1:
                filters_list["Pipeline"]["SKU"] = []
            if pipe_sub["plano_flag"] == 1:
                filters_list["Pipeline"]["PLANOGRAM"] = []
            if pipe_sub["asset_flag"] == 1:
                filters_list["Pipeline"]["ASSET"] = []
        else:
            filters_list["Pipeline"][pipelines[i]["pipeline_name"]] = []

    sql = "SELECT DISTINCT smd.shelf_sections FROM shelfwatch_dev.sw_shop_master_dump as smd WHERE smd.project_id=%s AND smd.td_version_id=%s"

    cursor.execute(sql, (project_pid, td_version_id))
    shelf_section = cursor.fetchall()

    shelf_list = []
    if len(shelf_section) > 0 and shelf_section[0] != None:
        for key in shelf_section[0]:
            shelf_list = shelf_section[0][key].split(',')
            shelf_dict = {}
            for key in shelf_list:
                shelf_dict[key] = []
            filters_list["Shelf Section"] = shelf_dict

    shop_master = {}
    shop_master["regular_osa"] = [0, 1]
    shop_master["npd_osa"] = [0, 1]
    filters_list["Shop Master"] = shop_master

    sql = "SELECT DISTINCT sta.attribute_name as a_name, sta.attribute_values as a_value FROM shelfwatch_dev.sw_td_attributes as sta WHERE sta.project_id=%s AND sta.td_version_id=%s"

    cursor.execute(sql, (project_pid, td_version_id))
    attributes = cursor.fetchall()

    attributes_names_list = []
    
    attribute_name = {}
    for key, value in itertools.groupby(attributes, lambda x: x["a_name"]):
        attribute_name[key] = []
        attributes_names_list.append(key)
        value_list = [i for i in value]
        for i in value_list:
            if i.get("a_value") not in attribute_name[key]:
                x = i.get("a_value")
                x = x[1:-1].split(", ")
                for t in x:
                    t.strip()
                    if t.strip('][').strip('""') not in attribute_name[key]:
                        attribute_name[key].append(t.strip('][').strip('""'))

    filters_list["Attributes"] = attribute_name

    config = {}
    config["filters"] = filters_list
    config["groups"] = {}
    # print(config)

    sql = "SELECT DISTINCT cl.class_name FROM shelfwatch_dev.sw_groups as g, shelfwatch_dev.sw_classes as cl WHERE cl.group_id=g.id AND cl.project_id=g.project_id AND cl.td_version_id=g.td_version_id \
        AND g.project_id=%s AND g.td_version_id=%s AND g.delete_status=0 AND cl.delete_status=0"

    cursor.execute(sql, (project_pid, td_version_id))
    classes = cursor.fetchall()

    groups_list = {"groups": {"Groups":[],"Classes": [], "Attributes": []}}

    # for key, val in itertools.groupby(classes, lambda x: x["class_name"]):
        # if key not in groups_list["groups"]["Classes"]:
        #     groups_list["groups"]["Classes"].append(key)

    groups_list["groups"]["Attributes"] = attributes_names_list
    config["groups"] = groups_list["groups"]

    
    # print(config)

    cursor.close()
    conn.close()
    return config



def get_shops_images_details(project_pid=72, td_version_id=80, date="2020-08-25 20:58:33"):
    date=datetime.strptime(date,'%Y-%m-%d %H:%M:%S').date()
    conn = db_connection()
    cursor = conn.cursor()
    
    # print(project_pid)
    sql = """
            SELECT
                user_id
            FROM
                sn_shop_category_status
            WHERE
                project_id=%s AND
                td_version_id=%s
            LIMIT 1
          """
    rows = cursor.execute(sql, (project_pid,td_version_id))
    if not rows:
        return {"user_data": []}

    user_data = cursor.fetchone()
    # print(user_data)
    if user_data['user_id']:
        sql = "SELECT * FROM sn_shop_category_status AS scs JOIN sn_users AS snu ON scs.user_id=snu.id WHERE scs.project_id=%s \
            AND scs.td_version_id=%s LIMIT 1"
        cursor.execute(sql, (project_pid,td_version_id))
        user_time_zone = cursor.fetchone()['time_zone']
        # print(user_time_zone)

        sql = """
                SELECT 
                    scs.assigned_sw_user_id, scs.capture_date, scs.category_id, c.category_name, scs.shop_category_id, shops.client_shop_id, 
                    scs.shop_id, IFNULL(shops.shop_name,shops.client_shop_id) as shop_name, scs.project_id, scs.td_version_id, 
                    snu.time_zone, u.user_name
                FROM 
                    sn_shop_category_status AS scs JOIN sw_users AS u ON u.id=scs.assigned_sw_user_id JOIN sw_td_shops 
                    AS shops ON shops.shop_id=scs.shop_id JOIN sw_categories AS c ON scs.category_id=c.id join sn_users as snu on scs.user_id=snu.id 
                WHERE 
                    scs.project_id=%s AND scs.td_version_id=%s AND DATE(CONVERT_TZ(scs.capture_date, '+00:00', snu.time_zone))=%s GROUP BY scs.shop_category_id ORDER BY 
                    scs.shop_category_id
              """
        cursor.execute(sql, (project_pid,td_version_id, date))
        user_data = cursor.fetchall()
        
        # print((user_data))

        for ud in user_data:
            sql = """
                    SELECT 
                        COUNT(id) AS image_count, created_at, scs.created_at 
                    FROM 
                        sw_test_images 
                    WHERE td_shop_id=%s 
                    AND category_id=%s 
                    AND DATE(CONVERT_TZ(created_at, '+00:00', %s))=%s 
                    AND delete_status=0
                    AND rtq=1
                    AND ai_quality_predict=1
                  """
            cursor.execute(sql, (ud['time_zone'], ud['shop_id'], ud['category_id'], ud['time_zone'], date))
            image_count = cursor.fetchone()['image_count']
            ud['image_count'] = 1 if image_count else 0
            ud['capture_date'] = str(ud['capture_date'])
            # print(ud)
    else:
        # print("Going into Else")
        sql = "SELECT * FROM sn_shop_category_status AS scs JOIN sw_api_users AS sau ON scs.api_user_id=sau.id WHERE scs.project_id=%s AND scs.td_version_id=%s LIMIT 1"
        cursor.execute(sql, (project_pid, td_version_id))
        user_time_zone = cursor.fetchone()['time_zone']

        sql = """
                SELECT
                    scs.assigned_sw_user_id, scs.capture_date, scs.category_id, c.category_name, scs.shop_category_id,
                    shops.client_shop_id, scs.shop_id, IFNULL(shops.shop_name,shops.client_shop_id) as shop_name, scs.project_id, scs.td_version_id, 
                    sau.time_zone, u.user_name
                FROM 
                    sn_shop_category_status AS scs JOIN sw_users AS u ON u.id=scs.assigned_sw_user_id JOIN sw_td_shops 
                    AS shops ON shops.shop_id=scs.shop_id JOIN sw_categories AS c ON scs.category_id=c.id JOIN sw_api_users AS sau ON scs.api_user_id=sau.id
                WHERE 
                    scs.project_id=%s AND scs.td_version_id=%s AND DATE(CONVERT_TZ(scs.created_at, '+00:00', sau.time_zone))=%s AND u.id=1 GROUP BY scs.shop_category_id ORDER BY 
                    scs.shop_category_id DESC
              """
        cursor.execute(sql, (project_pid, td_version_id, date))
        user_data = cursor.fetchall()

        # print(user_data)

        for ud in user_data:
            sql = """
                    SELECT 
                        COUNT(id) AS image_count, created_at, created_at 
                    FROM 
                        sw_test_images 
                    WHERE td_shop_id=%s 
                    AND category_id=%s 
                    AND DATE(CONVERT_TZ(created_at, '+00:00', %s))=%s 
                    AND delete_status=0
                    AND rtq=1
                    AND ai_quality_predict=1
                  """
            # print(ud['time_zone'])
            # print(date)
            cursor.execute(sql, (ud['shop_id'], ud['category_id'], ud['time_zone'], date))
            image_count = cursor.fetchone()['image_count']
            ud['image_count'] = 1 if image_count else 0
            ud['capture_date'] = (ud['capture_date'])

            # print(ud)
    cursor.close()
    conn.close()

    # print(" ")
    # print(user_data)
    # print(type(user_data[0]["capture_date"]))
    # print(len(user_data))   
    return {"user_data": user_data}


get_projects("4da91f3a-600d-4598-9e22-0ed34c35393b", "4ef000b2-c2e7-4622-a9c7-947642d6d5c9")
get_filters(project_pid=106,td_version_id=117)
get_shops_images_details()


delete_analytics_from_couchbase({"analytics_id":1, "project_id":2, "td_version_id":3})