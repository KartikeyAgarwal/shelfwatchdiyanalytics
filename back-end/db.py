import pymysql

def db_connection():
    connection = pymysql.connect(host='shelfwatch-2-dev.mysql.database.azure.com',
                                 user='dev_read_db',
                                 password='pd_dev_db_789',
                                 db='shelfwatch_dev',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor
                                 )
    return connection

# print(db_connection())