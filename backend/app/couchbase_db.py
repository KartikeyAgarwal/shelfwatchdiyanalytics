import couchbase
from couchbase.cluster import Cluster, ClusterOptions
from couchbase.cluster import PasswordAuthenticator

conn_url = "couchbase://localhost"
username="Admin"
cb_password="shelfwatch"

def couchdb_cluster():
    cluster = Cluster(conn_url, ClusterOptions(PasswordAuthenticator(username, cb_password)))
    return cluster