# import sys
# sys.path.insert(0,'back-end\app')
from collections import namedtuple
import re, json, uvicorn
from typing import Any, Text
from pymysql import cursors
from fastapi import FastAPI, APIRouter, Request
from fastapi.encoders import jsonable_encoder
from uuid import uuid4
import couchbase
from couchbase.cluster import Cluster, ClusterOptions
from couchbase.cluster import PasswordAuthenticator
import project_utils
from datetime import datetime

conn_url = "couchbase://localhost"
username="Admin"
cb_password="shelfwatch"

cluster = Cluster(conn_url, ClusterOptions(PasswordAuthenticator(username, cb_password)))
cb=cluster.bucket('DIY_Analytics')
cb_collection = cb.default_collection()

app = FastAPI()

# for analytics_details table on home page
@app.get('/{user_id}')
def get_analytics_details(user_id):
    table_data = project_utils.get_analytics_table_data(user_id)
    return table_data

# for projects list and td_version list on home page
@app.get('/')
def get_project_details():
    user_id="4da91f3a-600d-4598-9e22-0ed34c35393b" 
    role_id="4ef000b2-c2e7-4622-a9c7-947642d6d5c9"
    projects_info = project_utils.get_projects(user_id,role_id)
    return projects_info

# to validate analytics name in the home page
# response = True (name exists), response = False (name doesn't exist)
@app.post('/')
async def verify_analytics_details(request : Request):
    name = await request.body()
    analytics_name = name.decode()
    response = project_utils.validate_analytics_name_exists(analytics_name)
    return response 

# for analytics config on clicking create button, to validate and add to analytics table
@app.post('/')
async def create_analytics_config(request : Request):
    req = await request.json()
    name_exists = project_utils.validate_analytics_name_exists(req["analytics_name"])
    if name_exists == False:
        return False # "Analytics name already exists"
    
    validate_project_td= project_utils.validate_project_td_version(req["project_id"],req["td_version_id"],req["user_id"])
    if validate_project_td == False:
        return False # "Analytics for this project already exists"
    
    response1 = project_utils.add_to_analytics_table(req)
    response2 = project_utils.add_analytics_config(req)
    return response1 and response2

@app.delete('/')
async def delete_analytics(request: Request):
    req = await request.json()
    response1 = project_utils.delete_analytics_from_table(req)
    response2 = project_utils.delete_analytics_from_couchbase(req)
    return response1 and response2

# get the already configured analytics data from the couchbase based on
# key which has analytics_id, project_id, td_version_id, config_type
@app.get('/diy_analytics/{key}')
def get_analytics_config(key):
    resp = cb_collection.get(key)
    result = resp.content_as[dict]
    return result

# generate filters and groups config required on add_KPI page based on project_id and td_version_id and user_id
# key should have analytics_id, project_id, td_version_id, config_type
@app.get('/diy_analytics/{key}')
def generate_filters_config(key):
    print(key)
    key_list = key.split('_') # key_list = [analytics_id, project_id, td_version_id, config_type]
    project_id = int(key_list[1])
    td_version_id = int(key_list[2])
    config = project_utils.get_filters(project_id,td_version_id)
    return config

# post method to store the selected filters and groups into the couchbase on clicking submit on AddKPI page
@app.post('/diy_analytics/')
async def post_func(request: Request):
    req = await request.json()
    if req != None:
        key = "_".join([str(req["analytics_id"]), str(req["project_id"]), str(req["td_version_id"]),str(req["config_type"])])
        result = cb_collection.upsert(key, req)
        return (req,result.cas)
    else:
        x = request.get_data(as_text=True)
        key=x
        print(type(x))
        result = cb_collection.upsert(key, x)
        print("POST data is not json")
        return "POST request is not json"

# get the selected project and filters on the playgroud page
@app.get('/playground')
def get_filters_selected(key):
    resp = cb_collection.get(key)
    result = resp.content_as[dict]
    return result

# get method to get the shop, category and image count details
@app.get('/playground')
def get_shops_details():
    project_pid=72 
    td_version_id=80
    date="2020-08-25 20:58:33"
    details = project_utils.get_shops_images_details(project_pid, td_version_id, date)
    return details

# api.add_resource(Response,'/reportInsights/<key>')
# api.add_resource(Analytics,'/analytics/')

if __name__ == "__main__":
    uvicorn.run("user_apis:app")
