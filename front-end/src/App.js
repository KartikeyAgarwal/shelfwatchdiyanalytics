import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import DIYAnalyticsPage from "./pages/DIYAnalyticsPage";
import Playground from "./pages/Playground";
import ReviewSubmit from "./pages/ReviewSubmit";
import Home from "./pages/Home";
import Stepper from "./components/Stepper";
import AnalyticsHome from "./pages/AnalyticsHome";
import axios from "axios";

function App() {
  const [activeStep, setActiveStep] = useState(0);
  const [analyticsName, setAnalyticsName] = useState("");
  const [projectName, setProjectName] = useState("");
  const [kpiList, setKPIList] = useState([]);
  const [kpiGlobalList, setKPIGlobalList] = useState([]);

  const [finalData, setFinalData] = useState({});

  //SUBMIT MODAL
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  console.log("URLLL", window.location.href);

  return (
    <Router>
      {window.location.href === "http://localhost:3000/" ? (
        ""
      ) : (
        <Stepper
          activeStep={activeStep}
          setActiveStep={setActiveStep}
          handleClickOpen={handleClickOpen}
          finalData={finalData}
          setFinalData={setFinalData}
        />
      )}
      <div>
        <Switch>
          <Route exact path="/home" component={Home} />
          <Route exact path="/" component={AnalyticsHome} />
          <Route exact path="/diy_analytics">
            <DIYAnalyticsPage
              analyticsName={analyticsName}
              setAnalyticsName={setAnalyticsName}
              projectName={projectName}
              setProjectName={setProjectName}
              kpiList={kpiList}
              kpiGlobalList={kpiGlobalList}
              setKPIList={setKPIList}
              setKPIGlobalList={setKPIGlobalList}
              finalData={finalData}
              setFinalData={setFinalData}
            />
          </Route>
          <Route exact path="/playground">
            <Playground analyticsName={analyticsName} projectName={projectName} />
          </Route>
          <Route exact path="/review_and_submit">
            <ReviewSubmit
              open={open}
              handleClose={handleClose}
              analyticsName={analyticsName}
              projectName={projectName}
              kpiList={kpiList}
            />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
