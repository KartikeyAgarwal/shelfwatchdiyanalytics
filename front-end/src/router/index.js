import React from "react";
import { Navbar, Nav, Icon } from "rsuite";
import { Link } from "react-router-dom";

function index() {
  return (
    <Navbar>
      <Navbar.Header></Navbar.Header>
      <Navbar.Body>
        <Nav>
          <Link to="/home">
            <Nav.Item href="/home" icon={<Icon icon="home" />}>
              Home
            </Nav.Item>
          </Link>
          <Link to="/about">
            <Nav.Item>About</Nav.Item>
          </Link>
          <Link to="/users">
            <Nav.Item>Users</Nav.Item>
          </Link>
        </Nav>
      </Navbar.Body>
    </Navbar>
  );
}

export default index;
