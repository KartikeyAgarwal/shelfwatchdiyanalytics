import React, { useState, useRef, useEffect } from "react";
import { Collapse, Card, Button as BootButton } from "react-bootstrap";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import {
  MenuItem,
  FormControl,
  Select,
  TextField,
  InputLabel,
  IconButton,
  Button,
  Dialog,
  Typography,
} from "@material-ui/core/";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import Table from "./Table";
import "bootstrap/dist/css/bootstrap.min.css";
import "../App.css";
import AddIcon from "@material-ui/icons/Add";
import { v4 as uuidv4 } from "uuid";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import CancelIcon from "@material-ui/icons/Cancel";
import CloseIcon from "@material-ui/icons/Close";
import axios from "axios";

function AddDialog({
  onClose,
  open,
  newAnalyticsName,
  setNewAnalyticsName,
  projectName,
  setProjectName,
}) {
  const newAnalyticRef = useRef(null);
  const [projectList, setProjectList] = useState([]);
  const styles = (theme) => ({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });
  const useFormStyles = makeStyles((theme) => ({
    formControl: {
      minWidth: 160,
    },

    margin: {
      margin: theme.spacing(1),
    },
  }));

  const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
        <Typography variant="h6">{children}</Typography>
        {onClose ? (
          <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
            <CloseIcon />
          </IconButton>
        ) : null}
      </MuiDialogTitle>
    );
  });

  const DialogContent = withStyles((theme) => ({
    root: {
      padding: theme.spacing(2),
    },
  }))(MuiDialogContent);

  const DialogActions = withStyles((theme) => ({
    root: {
      margin: 0,
      padding: theme.spacing(1),
    },
  }))(MuiDialogActions);

  const handleCancel = () => {
    onClose();
  };
  const handleAdd = () => {
    setNewAnalyticsName(newAnalyticRef.current);
    onClose();
  };
  const formclasses = useFormStyles();

  var fetch_config = {
    method: "get",
    url: "https://2571dbd2-01d7-424b-b9b2-e4afa2d362d1.mock.pstmn.io/get_project_details",
    headers: {},
  };
  const FetchProjects = async () => {
    await axios(fetch_config)
      .then(function (response) {
        console.log("HERE IS UR DATA", response.data);
        setProjectList(response.data.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  // useEffect(() => {
  //   FetchProjects();
  // }, []);

  return (
    <Dialog
      // onEnter={FetchProjects}
      onClose={handleCancel}
      aria-labelledby="customized-dialog-title"
      open={open}
    >
      <DialogTitle id="customized-dialog-title" onClose={handleCancel}>
        Analytics Details
      </DialogTitle>
      <DialogContent dividers>
        <form>
          <div className="form-group row">
            <label className="col-sm-6 col-form-label" style={{}}>
              Analytics Name
            </label>
            <div className="col-sm-6">
              <TextField
                autoFocus
                ref={newAnalyticRef}
                onChange={(e) => (newAnalyticRef.current = e.target.value)}
              />
            </div>
          </div>
          <label className="col-sm-6 col-form-label" style={{}}>
            Proe Name
          </label>
          <FormControl
            style={{ wordWrap: "initial", marginLeft: "4%", marginTop: "-2.5%" }}
            className={formclasses.formControl}
          >
            <Select
              value={projectName}
              onChange={(e) => {
                setProjectName(e.target.value);
              }}
              displayEmpty
              inputProps={{ "aria-label": "Without label" }}
            >
              {projectList ? (
                projectList.map((item, index) => {
                  return (
                    <MenuItem key={item.project_id} value={item.project_name}>
                      {item.project_name}
                    </MenuItem>
                  );
                })
              ) : (
                <MenuItem value=""></MenuItem>
              )}
            </Select>
          </FormControl>
        </form>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleCancel} color="primary">
          Cancel
        </Button>
        <Button autoFocus onClick={handleAdd} color="primary">
          Create
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default function AnalyticsPallete() {
  const [rows, setRows] = useState([]);
  const [selected, setSelected] = useState([]);
  const [delButtonStatus, setDelButtonStatus] = React.useState(true);
  const [editButtonStatus, setEditButtonStatus] = React.useState(true);
  //   const [addButtonStatus, setAddButtonStatus] = React.useState(false);
  const [editId, setEditId] = React.useState();
  const [newAnalyticsName, setNewAnalyticsName] = useState("");
  const [projectName, setProjectName] = useState("");

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const useFormStyles = makeStyles((theme) => ({
    formControl: {
      minWidth: 160,
    },

    margin: {
      margin: theme.spacing(1),
    },
  }));
  const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    table: {
      // width: "50%",
    },
    margin: {
      margin: theme.spacing(1),
      marginTop: "3%",
    },
  }));
  const classes = useStyles();

  const saveRow = () => {
    var temp = {
      id: uuidv4(),
      //   report_kpi: reportKPI,
      //   aggregation_type: aggregationType,
      //   weightage: weightage,
    };
  };

  const editRow = (id) => {};

  const enableEdits = () => {};

  const deleteRow = (array) => {};

  const cancelEdit = () => {};

  return (
    <div style={{ marginTop: "5px" }}>
      <div>
        <Button
          style={{ margin: "2%" }}
          variant="outlined"
          onClick={handleClickOpen}
          startIcon={<AddIcon />}
        >
          New Analytics
        </Button>
        {!delButtonStatus && selected.length === 1 ? (
          <>
            <Button
              variant="contained"
              // color="inherit"
              onClick={() => editRow(editId)}
              className={classes.margin}
              startIcon={<CheckCircleIcon />}
            >
              Save
            </Button>
            <Button
              variant="contained"
              // color="inherit"
              onClick={() => cancelEdit()}
              className={classes.margin}
              startIcon={<CancelIcon />}
            >
              Cancel
            </Button>
          </>
        ) : (
          ""
        )}

        {selected.length > 0 && delButtonStatus ? (
          <IconButton
            disabled={false}
            className={classes.margin}
            onClick={() => {
              deleteRow(selected);
              setSelected([]);
            }}
          >
            <DeleteIcon />
          </IconButton>
        ) : (
          ""
        )}

        {selected.length === 1 && editButtonStatus ? (
          <IconButton
            disabled={false}
            className={classes.margin}
            onClick={() => {
              enableEdits();
            }}
          >
            <EditIcon />
          </IconButton>
        ) : (
          ""
        )}

        <div>
          <Table
            selected={selected}
            setSelected={setSelected}
            rows={rows}
            setRows={setRows}
            headCells={[
              { id: "sr_no", numeric: true, disablePadding: true, label: "Sr. No." },
              {
                id: "analytics_name",
                numeric: false,
                disablePadding: true,
                label: "Analytics Name",
              },
              { id: "project_name", numeric: false, disablePadding: true, label: "Project Name" },
              { id: "td_version_name", numeric: false, disablePadding: true, label: "TD Version" },
              { id: "last_saved", numeric: false, disablePadding: true, label: "Last Saved" },
            ]}
          />
        </div>

        <br></br>
        <div>
          <AddDialog
            onClose={onClose}
            open={open}
            newAnalyticsName={newAnalyticsName}
            setNewAnalyticsName={setNewAnalyticsName}
            projectName={projectName}
            setProjectName={setProjectName}
          />
        </div>
      </div>
    </div>
  );
}
