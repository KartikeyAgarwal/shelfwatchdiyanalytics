import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import { Stepper, Step, StepLabel, StepConnector, Button } from "@material-ui/core";
import Check from "@material-ui/icons/Check";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import AddIcon from "@material-ui/icons/Add";
import DirectionsRunIcon from "@material-ui/icons/DirectionsRun";
import PublishIcon from "@material-ui/icons/Publish";
import { Link } from "react-router-dom";

const useQontoStepIconStyles = makeStyles({
  root: {
    color: "#eaeaf0",
    display: "flex",
    height: 22,
    alignItems: "center",
  },
  active: {
    color: "#784af4",
  },
  circle: {
    width: 8,
    height: 8,
    borderRadius: "50%",
    backgroundColor: "currentColor",
  },
  completed: {
    color: "#784af4",
    zIndex: 1,
    fontSize: 18,
  },
});

function QontoStepIcon(props) {
  const classes = useQontoStepIconStyles();
  const { active, completed } = props;

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
      })}
    >
      {completed ? <Check className={classes.completed} /> : <div className={classes.circle} />}
    </div>
  );
}

QontoStepIcon.propTypes = {
  /**
   * Whether this step is active.
   */
  active: PropTypes.bool,
  /**
   * Mark the step as completed. Is passed to child components.
   */
  completed: PropTypes.bool,
};

const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    "& $line": {
      backgroundImage:
        "linear-gradient( 95deg, rgb(13,110,253) 0%, rgb(66, 209, 245) 50%, rgb(13,110,253) 100%)",
    },
  },
  completed: {
    "& $line": {
      backgroundImage:
        "linear-gradient( 95deg, rgb(13,110,253) 0%, rgb(66, 209, 245) 50%, rgb(13,110,253) 100%)",
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: "#eaeaf0",
    borderRadius: 1,
  },
})(StepConnector);

const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: "#ccc",
    zIndex: 1,
    color: "#fff",
    width: 45,
    height: 45,
    display: "flex",
    borderRadius: "25%",
    justifyContent: "center",
    alignItems: "center",
  },
  active: {
    backgroundImage:
      "linear-gradient( 136deg, rgb(13,110,253) 0%, rgb(66, 209, 245) 50%, rgb(13,110,253) 100%)",
    boxShadow: "0 4px 10px 0 rgba(0,0,0,.25)",
  },
  completed: {
    backgroundImage:
      "linear-gradient( 136deg, rgb(13,110,253) 0%, rgb(66, 209, 245) 50%, rgb(13,110,253) 100%)",
  },
});

function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles();
  const { active, completed } = props;

  const icons = {
    1: <AddIcon />,
    2: <DirectionsRunIcon />,
    3: <PublishIcon />,
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {icons[String(props.icon)]}
    </div>
  );
}

ColorlibStepIcon.propTypes = {
  /**
   * Whether this step is active.
   */
  active: PropTypes.bool,
  /**
   * Mark the step as completed. Is passed to child components.
   */
  completed: PropTypes.bool,
  /**
   * The label displayed in the step icon.
   */
  icon: PropTypes.node,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return ["ADD KPIs", "TEST RUN", "SUBMIT"];
}

export default function Stepper3Page(props) {
  const classes = useStyles();
  const steps = getSteps();
  const activeStep = props.activeStep;
  const setActiveStep = props.setActiveStep;
  const handleClickOpen = props.handleClickOpen;
  const finalData = props.finalData;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <div className={classes.root}>
      {activeStep === steps.length ? (
        <div
          style={{
            display: "inline-block",
            position: "absolute",
            left: 50,
            top: "10%",
            zIndex: 10,
          }}
        >
          <Button variant="outlined" color="primary" onClick={handleReset}>
            Reset
          </Button>
        </div>
      ) : (
        <div
          style={{
            display: "inline-block",
            position: "absolute",
            left: 40,
            top: "10%",
            zIndex: 10,
          }}
        >
          <Link
            to={activeStep === 1 ? "/diy_analytics" : activeStep === 2 ? "/playground" : ""}
            style={{ textDecoration: "none" }}
          >
            <Button
              variant="outlined"
              color="default"
              disabled={activeStep === 0}
              onClick={handleBack}
              startIcon={<NavigateBeforeIcon />}
            >
              Previous
            </Button>
          </Link>
        </div>
      )}

      <Stepper alternativeLabel activeStep={activeStep} connector={<ColorlibConnector />}>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>

      <div
        style={{
          display: "inline-block",
          position: "absolute",
          right: 50,
          top: "10%",
        }}
      >
        {activeStep !== steps.length ? (
          activeStep === steps.length - 1 ? (
            <Button
              endIcon={<NavigateNextIcon />}
              variant="outlined"
              color="secondary"
              onClick={handleClickOpen}
            >
              Submit
            </Button>
          ) : (
            <Link
              to={activeStep === 0 ? "/playground" : activeStep === 1 ? "/review_and_submit" : ""}
              style={{ textDecoration: "none" }}
            >
              <Button
                endIcon={<NavigateNextIcon />}
                variant="outlined"
                color="secondary"
                onClick={() => {
                  handleNext();
                  console.log(JSON.stringify(finalData));
                }}
              >
                Next
              </Button>
            </Link>
          )
        ) : (
          ""
        )}
      </div>
    </div>
  );
}
