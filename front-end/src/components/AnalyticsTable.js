import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
  Paper,
  Checkbox,
  Button,
  IconButton,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import CancelIcon from "@material-ui/icons/Cancel";
import AddCircleIcon from "@material-ui/icons/AddCircle";

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  { id: "sr_no", numeric: true, disablePadding: true, label: "Sr. No." },
  { id: "analytics_name", numeric: false, disablePadding: true, label: "Analytics Name" },
  { id: "project_name", numeric: false, disablePadding: true, label: "Project Name" },
  { id: "td_version_name", numeric: false, disablePadding: true, label: "TD Version" },
  { id: "last_saved", numeric: false, disablePadding: true, label: "Last Saved" },
];

function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell style={{ borderBottom: "none" }} padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ "aria-label": "select all desserts" }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            style={{ borderBottom: "none" }}
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            // padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: "2%",
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {},
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

export default function EnhancedTable(props) {
  const [selected, setSelected] = useState([]);
  const classes = useStyles();
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("filter-type");
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [rows, setRows] = useState([]);
  const [deleteButtonStatus, setDeleteButtonStatus] = useState(false);
  const [delButtonStatus, setDelButtonStatus] = useState(false);
  const [editButtonStatus, setEditButtonStatus] = useState(true);
  const [addButtonStatus, setAddButtonStatus] = useState(true);
  const [editId, setEditId] = useState();

  const saveRow = () => alert("Saved");
  const editRow = () => alert("Edited");
  const deleteRow = () => alert("Deleted");
  const cancelEdit = () => alert("Cancelled");
  const enableEdits = () => alert("Enabelled");

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.id);
      setSelected(newSelecteds);
      newSelecteds.length > 0 ? setDeleteButtonStatus(true) : setDeleteButtonStatus(false);
      return;
    }

    setSelected([]);
    setDeleteButtonStatus(false);
  };

  const handleClick = (event, id) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const isSelected = (name) => selected.indexOf(name) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  return (
    <div className={classes.root}>
      {addButtonStatus && delButtonStatus ? (
        <Button
          variant="contained"
          // color="inherit"
          onClick={() => saveRow()}
          className={classes.margin}
          startIcon={<AddCircleIcon />}
        >
          Add
        </Button>
      ) : (
        ""
      )}

      {!delButtonStatus && selected.length === 1 ? (
        <>
          <Button
            variant="contained"
            // color="inherit"
            onClick={() => editRow(editId)}
            className={classes.margin}
            startIcon={<CheckCircleIcon />}
          >
            Save
          </Button>
          <Button
            variant="contained"
            // color="inherit"
            onClick={() => cancelEdit()}
            className={classes.margin}
            startIcon={<CancelIcon />}
          >
            Cancel
          </Button>
        </>
      ) : (
        ""
      )}

      {selected.length > 0 && delButtonStatus ? (
        <IconButton
          disabled={false}
          className={classes.margin}
          onClick={() => {
            deleteRow(selected);
            setSelected([]);
          }}
        >
          <DeleteIcon />
        </IconButton>
      ) : (
        ""
      )}

      {selected.length === 1 && editButtonStatus ? (
        <IconButton
          disabled={false}
          className={classes.margin}
          onClick={() => {
            enableEdits();
          }}
        >
          <EditIcon />
        </IconButton>
      ) : (
        ""
      )}
      {/* <Paper className={classes.paper}> */}
      <TableContainer>
        <Table
          className={classes.table}
          aria-labelledby="tableTitle"
          size={"small"}
          aria-label="enhanced table"
        >
          <EnhancedTableHead
            classes={classes}
            numSelected={selected.length}
            order={order}
            orderBy={orderBy}
            onSelectAllClick={handleSelectAllClick}
            onRequestSort={handleRequestSort}
            rowCount={rows.length}
          />
          <TableBody>
            {stableSort(rows, getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => {
                const isItemSelected = isSelected(row.id);
                const labelId = `enhanced-table-checkbox-${index}`;

                return (
                  <TableRow
                    hover
                    onClick={(event) => handleClick(event, row.id)}
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={row.id}
                    selected={isItemSelected}
                  >
                    <TableCell style={{ borderBottom: "none" }} padding="checkbox">
                      <Checkbox
                        checked={isItemSelected}
                        inputProps={{ "aria-labelledby": labelId }}
                      />
                    </TableCell>
                    <TableCell
                      style={{ borderBottom: "none" }}
                      component="th"
                      id={labelId}
                      scope="row"
                      padding="none"
                    >
                      {row.filter_type}
                    </TableCell>
                    <TableCell style={{ borderBottom: "none" }}>{row.level_one}</TableCell>
                    <TableCell style={{ borderBottom: "none" }}>{row.level_two}</TableCell>
                  </TableRow>
                );
              })}
            {/* {emptyRows > 0 && (
                <TableRow>
                  <TableCell colSpan={4} />
                </TableRow>
              )} */}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      {/* </Paper> */}
    </div>
  );
}
