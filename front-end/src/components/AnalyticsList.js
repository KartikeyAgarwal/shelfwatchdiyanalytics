import React from "react";
import PropTypes from "prop-types";
import { FixedSizeList } from "react-window";
import { makeStyles } from "@material-ui/core/styles";
import { Button, Grid, Paper, ListItem, ListItemText } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    height: 400,
    // maxWidth: 300,
    backgroundColor: theme.palette.background.paper,
  },
}));

function ProjectList() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <FixedSizeList height={400} itemSize={46} itemCount={5}>
        {({ index, style }) => (
          <ListItem button style={style} key={index}>
            <ListItemText primary={`Item ${index + 1}`} />
            <ListItemText primary={`Item ${index + 1}`} />
            <ListItemText primary={`Item ${index + 1}`} />
          </ListItem>
        )}
      </FixedSizeList>
    </div>
  );
}

export default ProjectList;
