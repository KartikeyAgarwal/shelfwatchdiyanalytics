import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { GridListTile, GridList, Typography, IconButton, Dialog, Button } from "@material-ui/core";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import CloseIcon from "@material-ui/icons/Close";
import { makeStyles } from "@material-ui/core/styles";

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 500,
    height: 450,
  },
}));

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function ImagesModal(props) {
  const { setOpen, open, handleClose, handleClickOpen } = props;
  const classes = useStyles();

  const tileData = [
    {
      img: "",
      title: "Image",
      author: "author",
      cols: 2,
    },
    {
      img: "",
      title: "Image",
      author: "author",
      cols: 2,
    },
    {
      img: "",
      title: "Image",
      author: "author",
      cols: 2,
    },
    {
      img: "",
      title: "Image",
      author: "author",
      cols: 2,
    },
    {
      img: "",
      title: "Image",
      author: "author",
      cols: 2,
    },
    {
      img: "",
      title: "Image",
      author: "author",
      cols: 2,
    },
    {
      img: "",
      title: "Image",
      author: "author",
      cols: 2,
    },
  ];

  return (
    <div>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        {/* <DialogTitle id="customized-dialog-title" onClose={handleClose}> */}
        <DialogTitle id="customized-dialog-title">Images</DialogTitle>
        <DialogContent dividers>
          <div className={classes.root}>
            <GridList cellHeight={160} className={classes.gridList} cols={4}>
              {tileData.map((tile) => (
                <GridListTile key={tile.img} cols={tile.cols || 1}>
                  <img src={"https://picsum.photos/200/200"} alt={tile.title} />
                </GridListTile>
              ))}
            </GridList>
          </div>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
