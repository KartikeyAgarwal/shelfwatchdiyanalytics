import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Snackbar } from "@material-ui/core";
import "bootstrap/dist/css/bootstrap.min.css";
import MuiAlert from "@material-ui/lab/Alert";
import uuid from "react-uuid";
import NewKPI from "./newKPI";

export default function ReportInsights({ kpiList, setKPIList, finalData, setFinalData }) {
  const [flag, setFlag] = useState();
  const [saveBar, setSaveBar] = useState(false);
  const [delBar, setDelBar] = useState(false);
  const [showSubmit, setShowSubmit] = useState(false);

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  function handleAddKPI() {
    var temp = { id: uuid(), index: kpiList.length + 1, kpi_name: "", kpi_type: "", weightage: 0 };
    setKPIList((prevKpis) => {
      return [...prevKpis, temp];
    });
  }

  function deleteKPI(id, index) {
    setFlag(index);
    var k,
      temp = kpiList;
    temp = temp.filter((obj) => obj.id !== id);
    for (k = 1; k <= temp.length; k++) {
      temp[k - 1].index = k;
    }
    setKPIList(temp);
    console.log("AFTER DELETE:", temp);
    setDelBar(true);
    temp.length === 0 ? setShowSubmit(false) : setShowSubmit(true);
  }

  function saveKPI(id, saved_filters, saved_groups, name, type, basis) {
    var j,
      temp1 = kpiList;
    for (j = 0; j < temp1.length; j++) {
      if (temp1[j].id === id) {
        temp1[j]["filters"] = saved_filters;
        temp1[j]["group_by"] = saved_groups;
        temp1[j]["kpi_name"] = name;
        temp1[j]["kpi_type"] = { type: type, basis: basis };
        setFlag(temp1[j]["index"]);
      }
    }
    setKPIList([...temp1]);
    console.log("AFTER SAVE:", temp1);
    setSaveBar(true);
    setShowSubmit(true);
  }

  return (
    <div className="reportInsights card-body shadow">
      <div className="row">
        <h5 className="class-header w-100" style={{ marginBottom: "0.7rem" }}>
          Report Insights
        </h5>
        <br></br>
        <hr style={{ marginLeft: "2%", marginRight: "2%", width: "96%" }}></hr>
      </div>

      <div className="row w-70" style={{ marginLeft: "0.2%", width: "20%" }}>
        <Button
          onClick={handleAddKPI}
          size="sm"
          style={{ paddingRight: "0.1em", paddingLeft: "0.1em" }}
        >
          Add new KPI
        </Button>
      </div>

      <br></br>

      {kpiList.length > 0 ? (
        kpiList.map((item, index) => (
          <NewKPI
            key={item.id}
            id={item.id}
            index={item.index}
            saveKPI={saveKPI}
            deleteKPI={deleteKPI}
          />
        ))
      ) : (
        <></>
      )}
      {kpiList.length > 0 && showSubmit ? (
        <Button
          variant="success"
          style={{
            minWidth: "15%",
            marginTop: "5%",
            borderRadius: "3px",
            paddingRight: "0.5em",
            paddingLeft: "0.5em",
            marginLeft: "45%",
          }}
          onClick={() => {
            console.log("FINAL CONFIG", JSON.stringify({ config: kpiList }));
            setFinalData({ ...finalData, report_insights: kpiList });
          }}
        >
          Save
        </Button>
      ) : (
        ""
      )}

      {[
        { value: saveBar, change: setSaveBar, status: "success" },
        { value: delBar, change: setDelBar, status: "error" },
      ].map((item) => {
        return (
          <Snackbar
            open={item.value}
            autoHideDuration={3000}
            onClose={(event, reason) => {
              if (reason === "clickaway") {
                return;
              }
              item.change(false);
            }}
          >
            <Alert
              onClose={(event, reason) => {
                if (reason === "clickaway") {
                  return;
                }
                item.change(false);
              }}
              severity={item.status}
            >
              KPI {flag} {item.status === "success" ? "Saved" : "Deleted"}!
            </Alert>
          </Snackbar>
        );
      })}
    </div>
  );
}
