import React from "react";
import { FormControl, Select, InputLabel, MenuItem, IconButton, Button } from "@material-ui/core/";
import { makeStyles } from "@material-ui/core/styles";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import Table from "../../Table";
import { v4 as uuidv4 } from "uuid";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import CancelIcon from "@material-ui/icons/Cancel";

const filters = {
  filters: {
    Pipeline: [
      {
        SKU: [],
        POP: [],
        Planogram: [],
      },
    ],
    "Shop Master": [
      {
        regular_osa: [0, 1],
        npd_osa: [0, 1],
      },
    ],
    "Shelf Section": [{}],
    Attributes: [
      {
        Annotation_type: ["SKU", "Shelf"],
        Brand: ["Nestle"],
      },
    ],
  },
  groups: {
    Classes: [],
    Attributes: ["Brand", "SKU", "Orientation", "Segment"],
  },
};
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  table: {
    // width: "50%",
  },
  margin: {
    margin: theme.spacing(1),
    marginTop: "3%",
  },
}));

function FilterPallete(props) {
  const rows = props.rows;
  const setRows = props.setRows;
  const classes = useStyles();
  const [filter, setFilter] = React.useState("");
  const [levelOne, setLevelOne] = React.useState("");
  const [levelTwo, setLevelTwo] = React.useState("");
  const [addButtonStatus, setAddButtonStatus] = React.useState(false);
  const [filterOptions, setFilterOptions] = React.useState(Object.keys(filters["filters"]));
  const [levelOneOptions, setLevelOneOptions] = React.useState([]);
  const [levelTwoOptions, setLevelTwoOptions] = React.useState([]);
  const [levelOneVisibility, setLevelOneVisibility] = React.useState(false);
  const [levelTwoVisibility, setLevelTwoVisibility] = React.useState(false);

  const [selected, setSelected] = React.useState([]);
  const [delButtonStatus, setDelButtonStatus] = React.useState(true);
  const [editButtonStatus, setEditButtonStatus] = React.useState(true);
  const [editId, setEditId] = React.useState();
  const [deleteButtonStatus, setDeleteButtonStatus] = React.useState(false);

  const handleFilterChange = async (e) => {
    var content = Object.keys(filters["filters"][e.target.value][0]);
    if (content.length > 0) {
      setLevelOneOptions(content);
      setLevelOneVisibility(true);
      setAddButtonStatus(false);
    } else {
      setLevelOneOptions(content);
      setLevelOneVisibility(false);
      setAddButtonStatus(true);
    }
    setLevelOne("");
    setLevelTwo("");
    setFilter(e.target.value);
  };

  const handleLevelOneChange = (e) => {
    var levelone_content = filters["filters"][filter][0][e.target.value];
    if (Object.keys(levelone_content).length > 0) {
      setLevelTwoOptions(levelone_content);
      setLevelTwoVisibility(true);
      setAddButtonStatus(false);
    } else {
      setLevelTwoVisibility(false);
      setAddButtonStatus(true);
    }
    setLevelTwo("");
    setLevelOne(e.target.value);
  };

  const handleLevelTwoChange = (e) => {
    setLevelTwo(e.target.value);
    setAddButtonStatus(true);
  };

  function createData(id, filter_type, level_one, level_two) {
    return { id, filter_type, level_one, level_two };
  }

  const saveRow = () => {
    if (filter === "Attributes" && levelOne === "") {
      alert("Please Select Filter Level 1");
    } else {
      setRows([...rows, createData(uuidv4(), filter, levelOne, levelTwo)]);
      console.log(rows);
      setFilter("");
      setLevelOne("");
      setLevelTwo("");
      setLevelOneVisibility(false);
      setLevelTwoVisibility(false);
      setAddButtonStatus(false);
    }
  };

  const deleteRow = (array) => {
    var temp = rows;
    array.forEach((id) => {
      temp = temp.filter((obj) => obj.id !== id);
    });
    setRows(temp);
  };

  const enableEdits = () => {
    var temp = rows.filter((e) => e.id === selected[0]);
    setFilter(temp[0].filter_type);
    setLevelOne(temp[0].level_one ? temp[0].level_one : "");
    setLevelTwo(temp[0].level_two || temp[0].level_two === 0 ? temp[0].level_two : "");
    setEditId(temp[0].id);
    var content = filters["filters"][temp[0].filter_type][0];
    if (Object.keys(content).length > 0) {
      setLevelOneOptions(Object.keys(filters["filters"][temp[0].filter_type][0]));
      setLevelOneVisibility(true);
      if (filters["filters"][temp[0].filter_type][0][temp[0].level_one].length > 0) {
        setLevelTwoOptions(filters["filters"][temp[0].filter_type][0][temp[0].level_one]);
        setLevelTwoVisibility(true);
      } else {
        setLevelTwoVisibility(false);
      }
    } else {
      setLevelOneOptions(filters["filters"][temp[0].filter_type]);
      setLevelOneVisibility(false);
      setLevelTwoVisibility(false);
      setLevelOne("");
    }

    setDelButtonStatus(false);
    setEditButtonStatus(false);
    console.log(temp);
  };

  const editRow = (id) => {
    var temp = rows;
    temp.forEach((row) => {
      if (row.id === id) {
        row.filter_type = filter;
        row.level_one = levelOne ? levelOne : "";
        row.level_two = levelTwo ? levelTwo : "";
      }
    });
    setRows(temp);
    setFilter("");
    setLevelOne("");
    setLevelTwo("");
    setLevelOneVisibility(false);
    setLevelTwoVisibility(false);

    setAddButtonStatus(false);
    setEditButtonStatus(true);
    setDelButtonStatus(true);
    setSelected([]);
  };

  const cancelEdit = () => {
    var temp = rows.filter((e) => e.id === selected[0]);
    setFilter("");
    setLevelOne("");
    setLevelTwo("");
    setEditId("");
    setLevelOneOptions([]);
    setSelected([]);
    setLevelOneVisibility(false);
    setLevelTwoVisibility(false);
    setDelButtonStatus(true);
    setEditButtonStatus(true);
    console.log(temp);
  };

  return (
    <div style={{ flexDirection: "row" }}>
      <FormControl className={classes.formControl} size="medium">
        <InputLabel id="demo-simple-select-label">Filter Type</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={filter}
          onChange={handleFilterChange}
        >
          {filterOptions.map((item, index) => (
            <MenuItem key={index} value={item}>
              {item}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      {levelOneVisibility ? (
        <FormControl style={{ display: null }} className={classes.formControl} size="medium">
          <InputLabel id="demo-simple-select-label">{filter ? filter : ""}</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={levelOne}
            onChange={handleLevelOneChange}
          >
            {levelOneOptions
              ? levelOneOptions.map((item, index) => (
                  <MenuItem key={index} value={item}>
                    {item}
                  </MenuItem>
                ))
              : ""}
          </Select>
        </FormControl>
      ) : (
        ""
      )}
      {levelTwoVisibility ? (
        <FormControl style={{ display: null }} className={classes.formControl} size="medium">
          <InputLabel id="demo-simple-select-label">{levelOne ? levelOne : ""}</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={levelTwo}
            onChange={handleLevelTwoChange}
          >
            {levelTwoOptions
              ? levelTwoOptions.map((item, index) => (
                  <MenuItem key={index} value={item}>
                    {item}
                  </MenuItem>
                ))
              : ""}
          </Select>
        </FormControl>
      ) : (
        ""
      )}

      {addButtonStatus && delButtonStatus ? (
        <Button
          variant="contained"
          // color="inherit"
          onClick={() => saveRow()}
          className={classes.margin}
          startIcon={<AddCircleIcon />}
        >
          Add
        </Button>
      ) : (
        ""
      )}

      {!delButtonStatus && selected.length === 1 ? (
        <>
          <Button
            variant="contained"
            // color="inherit"
            onClick={() => editRow(editId)}
            className={classes.margin}
            startIcon={<CheckCircleIcon />}
          >
            Save
          </Button>
          <Button
            variant="contained"
            // color="inherit"
            onClick={() => cancelEdit()}
            className={classes.margin}
            startIcon={<CancelIcon />}
          >
            Cancel
          </Button>
        </>
      ) : (
        ""
      )}

      {selected.length > 0 && delButtonStatus ? (
        <IconButton
          disabled={false}
          className={classes.margin}
          onClick={() => {
            deleteRow(selected);
            setSelected([]);
          }}
        >
          <DeleteIcon />
        </IconButton>
      ) : (
        ""
      )}

      {selected.length === 1 && editButtonStatus ? (
        <IconButton
          disabled={false}
          className={classes.margin}
          onClick={() => {
            enableEdits();
          }}
        >
          <EditIcon />
        </IconButton>
      ) : (
        ""
      )}

      <div>
        <Table
          rows={rows}
          // deleteButtonStatus={deleteButtonStatus}
          // setDeleteButtonStatus={setDeleteButtonStatus}
          headCells={[
            { id: "filter_type", numeric: false, disablePadding: true, label: "Filter Type" },
            { id: "level_one", numeric: false, disablePadding: true, label: "Filter Level 1" },
            { id: "level_two", numeric: false, disablePadding: true, label: "Filter Level 2" },
          ]}
          selected={selected}
          setSelected={setSelected}
        />
      </div>
    </div>
  );
}

export default FilterPallete;
