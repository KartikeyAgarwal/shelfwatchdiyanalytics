import React from "react";
import FilterPallete from "./FilterPallete";
import "bootstrap/dist/css/bootstrap.min.css";
// import uuid from 'react-uuid'
import "../../../App.css";

export default function FilterReport({ filterRows, setFilterRows }) {
  return (
    <div>
      <div
        className="card-body"
        style={{
          border: "1px solid lightgrey",
          borderRadius: "3px",
          backgroundColor: "#F9F9F9",
        }}
      >
        <div className="row filterKpi">
          <h6 className="class-header w-100" style={{ marginBottom: "0.7rem" }}>
            Filters
          </h6>
          <br></br>

          <hr
            style={{
              height: "1px",
              marginLeft: "2%",
              marginRight: "2%",
              width: "96%",
            }}
          ></hr>

          <FilterPallete rows={filterRows} setRows={setFilterRows} />
        </div>
      </div>
    </div>
  );
}

// [{f:'',l1:'',l2:''}, {f:'', l1:'',l2:''}]
