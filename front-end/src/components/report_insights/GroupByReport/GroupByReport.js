import React from "react";
import GroupByPallete from "./GroupByPallete";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../../App.css";

export default function GroupByReport({ groupByRows, setGroupByRows }) {
  return (
    <div>
      <div
        className="card-body"
        style={{
          border: "1px solid lightgrey",
          borderRadius: "3px",
          marginTop: "8px",
          backgroundColor: "#F9F9F9",
        }}
      >
        <div className="row groupKpi">
          <h6 className="class-header w-100" style={{ marginBottom: "0.7rem" }}>
            Group by
          </h6>
          <br></br>

          <hr
            style={{
              height: "0.5px",
              marginLeft: "2%",
              marginRight: "2%",
              width: "96%",
            }}
          ></hr>
          <GroupByPallete rows={groupByRows} setRows={setGroupByRows} />
        </div>
      </div>
    </div>
  );
}
