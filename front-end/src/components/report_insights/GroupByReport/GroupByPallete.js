import React from "react";
import { FormControl, Select, InputLabel, MenuItem, IconButton, Button } from "@material-ui/core/";
import { makeStyles } from "@material-ui/core/styles";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import Table from "../../Table";
import { v4 as uuidv4 } from "uuid";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import CancelIcon from "@material-ui/icons/Cancel";

const filters = {
  filters: {
    Pipeline: [
      {
        SKU: [],
        POP: [],
        Planogram: [],
      },
    ],
    "Shop Master": [
      {
        regular_osa: [0, 1],
        npd_osa: [0, 1],
      },
    ],
    "Shelf Section": [{}],
    Attributes: [
      {
        Annotation_type: ["SKU", "Shelf"],
        Brand: ["Nestle"],
      },
    ],
  },
  groups: {
    Classes: [],
    Attributes: ["Brand", "SKU", "Orientation", "Segment"],
  },
};
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  table: {
    // width: "50%",
  },
  margin: {
    margin: theme.spacing(1),
    marginTop: "3%",
  },
}));

function GroupByPallete(props) {
  const rows = props.rows;
  const setRows = props.setRows;
  const classes = useStyles();
  const [group, setGroup] = React.useState("");
  const [levelOne, setLevelOne] = React.useState("");
  const [addButtonStatus, setAddButtonStatus] = React.useState(false);
  const [groupOptions, setGroupOptions] = React.useState(Object.keys(filters["groups"]));
  const [levelOneOptions, setLevelOneOptions] = React.useState([]);
  const [levelOneVisibility, setLevelOneVisibility] = React.useState(false);

  const [selected, setSelected] = React.useState([]);
  const [delButtonStatus, setDelButtonStatus] = React.useState(true);
  const [editButtonStatus, setEditButtonStatus] = React.useState(true);
  const [editId, setEditId] = React.useState();
  const [deleteButtonStatus, setDeleteButtonStatus] = React.useState(false);

  const handleGroupChange = async (e) => {
    if (filters["groups"][e.target.value].length > 0) {
      setLevelOneOptions(filters["groups"][e.target.value]);
      setLevelOneVisibility(true);
      setAddButtonStatus(false);
      setGroup(e.target.value);
    } else {
      setLevelOneOptions(filters["groups"][e.target.value]);
      setLevelOneVisibility(false);
      setLevelOne("");
      setAddButtonStatus(true);
      setGroup(e.target.value);
    }
  };

  const handleLevelOneChange = (e) => {
    setLevelOne(e.target.value);
    setAddButtonStatus(true);
  };

  function createData(id, group, level_one) {
    return { id, group, level_one };
  }

  const saveRow = () => {
    if (group === "Attributes" && levelOne === "") {
      alert("Please Select Filter Level 1");
    } else {
      setRows([...rows, createData(uuidv4(), group, levelOne)]);
      setGroup("");
      setLevelOne("");
      setLevelOneVisibility(false);
      setAddButtonStatus(false);
    }
    console.log("GROW", rows);
  };

  const deleteRow = (array) => {
    var temp = rows;
    array.forEach((id) => {
      temp = temp.filter((obj) => obj.id !== id);
    });
    setRows(temp);
    console.log("TEMP", temp);
    console.log(array);
  };

  const enableEdits = () => {
    var temp = rows.filter((e) => e.id === selected[0]);
    setGroup(temp[0].group);
    setLevelOne(temp[0].level_one);
    setEditId(temp[0].id);

    if (filters["groups"][temp[0].group].length > 0) {
      setLevelOneOptions(filters["groups"][temp[0].group]);
      setLevelOneVisibility(true);
    } else {
      setLevelOneOptions(filters["groups"][temp[0].group]);
      setLevelOneVisibility(false);
      setLevelOne("");
    }

    setDelButtonStatus(false);
    setEditButtonStatus(false);
    console.log(temp);
  };

  const editRow = (id) => {
    var temp = rows;
    temp.forEach((row) => {
      if (row.id === id) {
        row.group = group;
        row.level_one = levelOne ? levelOne : "";
      }
    });
    setRows(temp);
    setGroup("");
    setLevelOne("");
    setLevelOneVisibility(false);
    setAddButtonStatus(false);
    setEditButtonStatus(true);
    setDelButtonStatus(true);
    setSelected([]);
  };

  const cancelEdit = () => {
    var temp = rows.filter((e) => e.id === selected[0]);
    setGroup("");
    setLevelOne("");
    setEditId("");
    setLevelOneOptions([]);
    setSelected([]);
    setLevelOneVisibility(false);
    setDelButtonStatus(true);
    setEditButtonStatus(true);
    console.log(temp);
  };

  return (
    <div style={{ flexDirection: "row" }}>
      <FormControl className={classes.formControl} size="medium">
        <InputLabel id="demo-simple-select-label">Groups</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={group}
          onChange={handleGroupChange}
        >
          {groupOptions.map((item, index) => (
            <MenuItem key={index} value={item}>
              {item}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      {levelOneVisibility ? (
        <FormControl style={{ display: null }} className={classes.formControl} size="medium">
          <InputLabel id="demo-simple-select-label">{group ? group : ""}</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={levelOne}
            onChange={handleLevelOneChange}
          >
            {levelOneOptions
              ? levelOneOptions.map((item, index) => (
                  <MenuItem key={index} value={item}>
                    {item}
                  </MenuItem>
                ))
              : ""}
          </Select>
        </FormControl>
      ) : (
        ""
      )}

      {addButtonStatus && delButtonStatus ? (
        <Button
          variant="contained"
          // color="inherit"
          onClick={() => saveRow()}
          className={classes.margin}
          startIcon={<AddCircleIcon />}
        >
          Add
        </Button>
      ) : (
        ""
      )}
      {!delButtonStatus && selected.length === 1 ? (
        <>
          <Button
            variant="contained"
            // color="inherit"
            onClick={() => editRow(editId)}
            className={classes.margin}
            startIcon={<CheckCircleIcon />}
          >
            Save
          </Button>
          <Button
            variant="contained"
            // color="inherit"
            onClick={() => cancelEdit()}
            className={classes.margin}
            startIcon={<CancelIcon />}
          >
            Cancel
          </Button>
        </>
      ) : (
        ""
      )}

      {selected.length > 0 && delButtonStatus ? (
        <IconButton
          disabled={false}
          className={classes.margin}
          onClick={() => {
            deleteRow(selected);
            setSelected([]);
          }}
        >
          <DeleteIcon />
        </IconButton>
      ) : (
        ""
      )}

      {selected.length === 1 && editButtonStatus ? (
        <IconButton
          disabled={false}
          className={classes.margin}
          onClick={() => {
            enableEdits();
          }}
        >
          <EditIcon />
        </IconButton>
      ) : (
        ""
      )}

      <div>
        <Table
          rows={rows}
          // deleteButtonStatus={deleteButtonStatus}
          // setDeleteButtonStatus={setDeleteButtonStatus}
          headCells={[
            { id: "group", numeric: false, disablePadding: true, label: "Groups" },
            { id: "level_one", numeric: false, disablePadding: true, label: "Group Level 1" },
          ]}
          selected={selected}
          setSelected={setSelected}
        />
      </div>
    </div>
  );
}

export default GroupByPallete;
