import React, { useState } from "react";
import { Button, Collapse, Card } from "react-bootstrap";
import { makeStyles } from "@material-ui/core/styles";
import { MenuItem, FormControl, Select, TextField } from "@material-ui/core/";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../App.css";

import FilterReport from "./FilterReport/FilterReport";
import GroupByReport from "./GroupByReport/GroupByReport";

export default function NewKPI({ id, index, saveKPI, deleteKPI }) {
  const [filterRows, setFilterRows] = React.useState([]);
  const [groupByRows, setGroupByRows] = React.useState([]);
  const [kpiName, setKPIName] = useState("");
  const [isVisible, setisVisible] = useState(false);
  const [kpiType, setKPIType] = useState("");
  const [kpiBasisVisibility, setKPIBasisVisibility] = useState(false);
  const [kpiBasis, setKPIBasis] = useState("");

  const useFormStyles = makeStyles((theme) => ({
    formControl: {
      minWidth: 120,
    },

    margin: {
      margin: theme.spacing(1),
    },
  }));
  const formclasses = useFormStyles();

  function onClickKPI(e) {
    setisVisible(!isVisible);
    return;
  }

  const KPIType = [
    { id: "1", label: "SOS" },
    { id: "2", label: "OSA" },
  ];

  const KPIBasis = [
    { id: "1", label: "Pixel-based" },
    { id: "2", label: "Count-based" },
  ];

  return (
    <div style={{ marginTop: "5px" }}>
      <Card>
        <Card.Header
          onClick={onClickKPI}
          style={{
            wordWrap: "initial",
          }}
        >
          <div style={{ flexDirection: "row" }}>
            <p
              style={{
                display: "inline-block",
                marginBottom: "-1%",
              }}
            >
              {kpiName ? kpiName : "KPI"}
            </p>
            <i
              className="arrow down"
              style={{
                marginTop: "1%",
                transform: "rotate(45deg)",
                WebkitTransform: "rotate(45deg)",
                position: "absolute",
                right: "5%",
              }}
            ></i>
          </div>
        </Card.Header>

        <Collapse in={isVisible}>
          <Card.Body style={{ backgroundColor: "#F5F5F5" }}>
            <div id="example-collapse-text" style={{ flexDirection: "row" }}>
              <form>
                <div className="form-group row">
                  <label className="col-sm-2 col-form-label" style={{ wordWrap: "initial" }}>
                    KPI Name
                  </label>
                  <div className="col-sm-10">
                    <TextField
                      value={kpiName}
                      onChange={(e) => setKPIName(e.target.value)}
                      // placeholder="KPI Name"
                    />
                  </div>
                </div>
              </form>

              <br></br>

              <form>
                <div className="form-group row">
                  <label className="col-sm-2 col-form-label" style={{ wordWrap: "initial" }}>
                    KPI Type
                  </label>
                  <div className="col-sm-3" style={{ lineHeight: "1.5em" }}>
                    <FormControl className={formclasses.formControl}>
                      <Select
                        value={kpiType}
                        onChange={(e) => {
                          setKPIType(e.target.value);
                          if (e.target.value === "SOS") {
                            setKPIBasisVisibility(true);
                          } else {
                            setKPIBasisVisibility(false);
                            setKPIBasis("");
                          }
                        }}
                        displayEmpty
                        inputProps={{ "aria-label": "Without label" }}
                      >
                        <MenuItem value="">Select</MenuItem>
                        {KPIType.map((item, index) => (
                          <MenuItem key={item.id} value={item.label}>
                            {item.label}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </div>

                  {kpiBasisVisibility ? (
                    <div className="col-sm-3" style={{ lineHeight: "1.5em" }}>
                      <FormControl className={formclasses.formControl}>
                        <Select
                          value={kpiBasis}
                          onChange={(e) => setKPIBasis(e.target.value)}
                          displayEmpty
                          inputProps={{ "aria-label": "Without label" }}
                        >
                          <MenuItem value="">Select</MenuItem>
                          {KPIBasis.map((item, index) => (
                            <MenuItem key={item.id} value={item.label}>
                              {item.label}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </form>

              <br></br>

              {/* "<-------------------------->" */}
              <FilterReport filterRows={filterRows} setFilterRows={setFilterRows} />

              <GroupByReport groupByRows={groupByRows} setGroupByRows={setGroupByRows} />

              <div
                style={{
                  display: "flex",
                  flexDirection: "row-reverse",
                  marginLeft: "auto",
                }}
              >
                <Button
                  size="sm"
                  style={{
                    minWidth: "15%",
                    marginTop: "10px",
                    borderRadius: "3px",
                    paddingRight: "0.5em",
                    paddingLeft: "0.5em",
                    marginLeft: "0.5em",
                  }}
                  onClick={() => {
                    saveKPI(id, filterRows, groupByRows, kpiName, kpiType, kpiBasis);
                    setisVisible(!isVisible);
                  }}
                >
                  Save KPI
                </Button>
                <Button
                  variant="danger"
                  size="sm"
                  style={{
                    minWidth: "15%",
                    marginTop: "10px",
                    borderRadius: "3px",
                    paddingRight: "0.5em",
                    paddingLeft: "0.5em",
                  }}
                  onClick={() => deleteKPI(id, index)}
                >
                  Delete KPI
                </Button>
              </div>
            </div>
          </Card.Body>
        </Collapse>
      </Card>
    </div>
  );
}
