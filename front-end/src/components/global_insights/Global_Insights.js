import React, { useState, useRef } from "react";
import { Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TableHead,
  Paper,
  TextField,
  IconButton,
} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import CreateIcon from "@material-ui/icons/Create";
import NewGlobalKPI from "./NewGlobalKPI";
import uuid from "react-uuid";

function createData(name, type, weightage) {
  return { name, type, weightage };
}

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 16,
  },
}))(TableCell);

const rows = [
  createData("Frozen yoghurt", "SOS", 6.0),
  createData("Ice cream sandwich", "OSA", 9.0),
];

const useStyles = makeStyles({
  table: {
    minWidth: "50%",
  },
});

export default function GlobalInsights({
  kpiGlobalList,
  setKPIGlobalList,
  kpiList,
  finalData,
  setFinalData,
}) {
  const weight = useRef(null);
  const [total, setTotal] = useState(100);
  const [showSubmit, setShowSubmit] = useState(false);
  const [flag, setFlag] = useState();
  const [saveBar, setSaveBar] = useState(false);
  const [delBar, setDelBar] = useState(false);

  function handleAddGlobalKPI() {
    var temp = {
      id: uuid(),
      index: kpiGlobalList.length + 1,
      global_insight_name: "",
    };
    setKPIGlobalList((prevKpis) => {
      return [...prevKpis, temp];
    });
  }

  function deleteGlobalKPI(id, index) {
    setFlag(index);
    var k,
      temp = kpiGlobalList;
    temp = temp.filter((obj) => obj.id !== id);
    for (k = 1; k <= temp.length; k++) {
      temp[k - 1].index = k;
    }
    setKPIGlobalList(temp);
    console.log("AFTER DELETE:", temp);
    setDelBar(true);
    temp.length === 0 ? setShowSubmit(false) : setShowSubmit(true);
  }

  function saveGlobalKPI(id, name, list) {
    var j,
      temp1 = kpiGlobalList;
    for (j = 0; j < temp1.length; j++) {
      if (temp1[j].id === id) {
        temp1[j]["global_insight_name"] = name;
        temp1[j]["report_kpi_list"] = list;
        setFlag(temp1[j]["index"]);
      }
    }
    setKPIGlobalList([...temp1]);
    console.log("AFTER SAVE:", temp1);
    setSaveBar(true);
    setShowSubmit(true);
  }

  const AddWeightage = (id) => {
    var w;
    kpiGlobalList.forEach((o) => {
      if (o.id === id) w = o.weightage;
    });
    console.log("W IDS", w, id);
    var temp1 = kpiGlobalList;
    var original = kpiGlobalList;
    var s = 0;
    temp1 = temp1.filter((o) => o.id !== id);
    temp1.forEach((d) => (s = s + d.weightage));
    if (s + w > total) {
      let extra = Math.abs(total - (s + w));
      original.forEach((d) => {
        if (d.id !== id) {
          d.weightage =
            d.weightage > 0 && d.weightage - extra / temp1.length > 0
              ? d.weightage - extra / temp1.length
              : 0;
        } else {
          d.weightage = w;
        }
      });
    } else {
      for (let j = 0; j < original.length; j++) {
        if (original[j].id === id) {
          original[j]["weightage"] = w;
        }
      }
    }

    console.log("SS", s);
    console.log("TOTAL", total);
    setKPIGlobalList([...original]);
  };

  const classes = useStyles();
  console.log("HELO FROM GLOBAL");
  console.log("KPI IN GLOBAL", kpiGlobalList);

  return (
    <div className="globalInsights card-body shadow">
      <div className="row">
        <h5 className="class-header w-100"> Global Insights </h5>
        <hr style={{ marginLeft: "2%", marginRight: "2%", width: "96%" }}></hr>
      </div>

      <div className="row w-70" style={{ marginLeft: "0.2%", width: "30%" }}>
        <Button
          onClick={() => {
            if (kpiList.length > 0) {
              handleAddGlobalKPI();
            } else {
              alert("Please Add a KPI first.");
            }
          }}
          size="sm"
          style={{ paddingRight: "0.1em", paddingLeft: "0.1em" }}
        >
          Add new Global KPI
        </Button>
      </div>

      <br></br>

      {kpiGlobalList.length > 0 ? (
        kpiGlobalList.map((item, index) => (
          <NewGlobalKPI
            key={item.id}
            id={item.id}
            index={item.index}
            saveGlobalKPI={saveGlobalKPI}
            deleteGlobalKPI={deleteGlobalKPI}
            kpiGlobalList={kpiGlobalList}
            kpiList={kpiList}
          />
        ))
      ) : (
        <></>
      )}
      {kpiGlobalList.length > 0 && showSubmit ? (
        <Button
          variant="success"
          style={{
            minWidth: "15%",
            marginTop: "5%",
            borderRadius: "3px",
            paddingRight: "0.5em",
            paddingLeft: "0.5em",
            marginLeft: "45%",
          }}
          onClick={() => {
            console.log("FINAL CONFIG", JSON.stringify({ config: kpiGlobalList }));
            setFinalData({ ...finalData, global_insights: kpiGlobalList });
          }}
        >
          Save
        </Button>
      ) : (
        ""
      )}

      {/* <div className="row"> */}
      {/* </div> */}
    </div>
  );
}
