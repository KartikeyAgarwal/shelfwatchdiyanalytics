import React, { useState } from "react";
import { Collapse, Card, Button as BootButton } from "react-bootstrap";
import { makeStyles } from "@material-ui/core/styles";
import {
  MenuItem,
  FormControl,
  Select,
  TextField,
  InputLabel,
  IconButton,
  Button,
} from "@material-ui/core/";
import Table from "../Table";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../App.css";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import { v4 as uuidv4 } from "uuid";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import CancelIcon from "@material-ui/icons/Cancel";

export default function NewGlobalKPI({ id, index, saveGlobalKPI, deleteGlobalKPI, kpiList }) {
  const [rows, setRows] = useState([]);
  const [selected, setSelected] = useState([]);
  const [globalInsightName, setGlobalInsightName] = useState("");
  const [isVisible, setisVisible] = useState(false);

  const [reportKPI, setReportKPI] = useState("");
  const [aggregationType, setAggregationType] = useState("");
  const [weightage, setWeightage] = useState("");

  const [delButtonStatus, setDelButtonStatus] = React.useState(true);
  const [editButtonStatus, setEditButtonStatus] = React.useState(true);
  const [deleteButtonStatus, setDeleteButtonStatus] = React.useState(false);
  const [addButtonStatus, setAddButtonStatus] = React.useState(false);
  const [editId, setEditId] = React.useState();

  const useFormStyles = makeStyles((theme) => ({
    formControl: {
      minWidth: 160,
    },

    margin: {
      margin: theme.spacing(1),
    },
  }));

  const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    table: {
      // width: "50%",
    },
    margin: {
      margin: theme.spacing(1),
      marginTop: "3%",
    },
  }));

  const formclasses = useFormStyles();
  const classes = useStyles();

  function onClickKPI(e) {
    setisVisible(!isVisible);
    return;
  }

  const saveRow = () => {
    var temp = {
      id: uuidv4(),
      report_kpi: reportKPI,
      aggregation_type: aggregationType,
      weightage: weightage,
    };
    setRows([...rows, temp]);
    setReportKPI("");
    setAggregationType("");
    setWeightage("");
  };

  const editRow = (id) => {
    var temp = rows;
    temp.forEach((row) => {
      if (row.id === id) {
        row.report_kpi = reportKPI;
        row.aggregation_type = aggregationType;
        row.weightage = weightage;
      }
    });
    setRows(temp);
    setReportKPI("");
    setAggregationType("");
    setWeightage("");
    setAddButtonStatus(false);
    setEditButtonStatus(true);
    setDelButtonStatus(true);
    setSelected([]);
  };

  const enableEdits = () => {
    var temp = rows.filter((e) => e.id === selected[0]);
    setReportKPI(temp[0].report_kpi);
    setAggregationType(temp[0].aggregation_type);
    setWeightage(temp[0].weightage);
    setEditId(temp[0].id);

    setDelButtonStatus(false);
    setEditButtonStatus(false);
  };

  const deleteRow = (array) => {
    var temp = rows;
    array.forEach((id) => {
      temp = temp.filter((obj) => obj.id !== id);
    });
    setRows(temp);
  };

  const cancelEdit = () => {
    var temp = rows.filter((e) => e.id === selected[0]);
    setReportKPI("");
    setAggregationType("");
    setWeightage("");
    setEditId("");
    setSelected([]);
    setDelButtonStatus(true);
    setEditButtonStatus(true);
  };

  return (
    <div style={{ marginTop: "5px" }}>
      <Card>
        <Card.Header
          onClick={onClickKPI}
          style={{
            wordWrap: "initial",
          }}
        >
          <div style={{ flexDirection: "row" }}>
            <p
              style={{
                display: "inline-block",
                marginBottom: "-1%",
              }}
            >
              {globalInsightName ? globalInsightName : "Global Insight"}
            </p>
            <i
              className="arrow down"
              style={{
                marginTop: "1%",
                transform: "rotate(45deg)",
                WebkitTransform: "rotate(45deg)",
                position: "absolute",
                right: "5%",
              }}
            ></i>
          </div>
        </Card.Header>

        <Collapse in={isVisible}>
          <Card.Body style={{ backgroundColor: "#F5F5F5" }}>
            <div id="example-collapse-text" style={{ flexDirection: "row" }}>
              <form>
                <div className="form-group row">
                  <label className="col-sm-4 col-form-label" style={{}}>
                    Global Insight Name
                  </label>
                  <div className="col-sm-8">
                    <TextField
                      value={globalInsightName}
                      onChange={(e) => setGlobalInsightName(e.target.value)}
                      // placeholder="KPI Name"
                    />
                  </div>
                </div>
              </form>

              <br></br>

              <form>
                <div className="form-group row">
                  <div style={{ marginTop: "2%" }}>
                    {/* <label className="col-sm-2 col-form-label" style={{ wordWrap: "initial" }}>
                      Report KPI
                    </label> */}
                    <FormControl
                      style={{ wordWrap: "initial", marginLeft: "4%", marginTop: "-2.5%" }}
                      className={formclasses.formControl}
                    >
                      <InputLabel id="demo-simple-select-label">Report KPI</InputLabel>
                      <Select
                        value={reportKPI}
                        onChange={(e) => {
                          setReportKPI(e.target.value);
                        }}
                        displayEmpty
                        inputProps={{ "aria-label": "Without label" }}
                      >
                        {kpiList ? (
                          kpiList.map((item, index) => {
                            return (
                              <MenuItem key={item.id} value={item.kpi_name}>
                                {item.kpi_name}
                              </MenuItem>
                            );
                          })
                        ) : (
                          <MenuItem value=""></MenuItem>
                        )}
                      </Select>
                    </FormControl>

                    <FormControl
                      className="col-sm-3 col-form-label"
                      style={{ wordWrap: "initial", marginLeft: "4%", marginTop: "-2.5%" }}
                      className={formclasses.formControl}
                    >
                      <InputLabel id="demo-simple-select-label">Aggregation Type</InputLabel>
                      <Select
                        value={aggregationType}
                        onChange={(e) => {
                          setAggregationType(e.target.value);
                        }}
                        displayEmpty
                        // inputProps={{ "aria-label": "Without label" }}
                      >
                        <MenuItem value="Median">Median</MenuItem>
                        <MenuItem value="Average">Average</MenuItem>
                        <MenuItem value="Sum">Sum</MenuItem>
                      </Select>
                    </FormControl>

                    <TextField
                      style={{ height: 20, width: 80, marginLeft: "4%", marginTop: "-2%" }}
                      size={"small"}
                      value={weightage}
                      label={"Weightage"}
                      onChange={(e) => setWeightage(e.target.value)}
                    />
                    {" %"}
                  </div>
                </div>
              </form>

              {reportKPI && aggregationType && weightage && delButtonStatus ? (
                <Button
                  variant="contained"
                  // color="inherit"
                  onClick={() => saveRow()}
                  className={classes.margin}
                  startIcon={<AddCircleIcon />}
                >
                  Add
                </Button>
              ) : (
                ""
              )}
              {!delButtonStatus && selected.length === 1 ? (
                <>
                  <Button
                    variant="contained"
                    // color="inherit"
                    onClick={() => editRow(editId)}
                    className={classes.margin}
                    startIcon={<CheckCircleIcon />}
                  >
                    Save
                  </Button>
                  <Button
                    variant="contained"
                    // color="inherit"
                    onClick={() => cancelEdit()}
                    className={classes.margin}
                    startIcon={<CancelIcon />}
                  >
                    Cancel
                  </Button>
                </>
              ) : (
                ""
              )}

              {selected.length > 0 && delButtonStatus ? (
                <IconButton
                  disabled={false}
                  className={classes.margin}
                  onClick={() => {
                    deleteRow(selected);
                    setSelected([]);
                  }}
                >
                  <DeleteIcon />
                </IconButton>
              ) : (
                ""
              )}

              {selected.length === 1 && editButtonStatus ? (
                <IconButton
                  disabled={false}
                  className={classes.margin}
                  onClick={() => {
                    enableEdits();
                  }}
                >
                  <EditIcon />
                </IconButton>
              ) : (
                ""
              )}

              <div>
                <Table
                  selected={selected}
                  setSelected={setSelected}
                  rows={rows}
                  setRows={setRows}
                  headCells={[
                    { id: "report_kpi", numeric: false, disablePadding: true, label: "Report KPI" },
                    {
                      id: "aggregation_type",
                      numeric: false,
                      disablePadding: true,
                      label: "Aggregation Type",
                    },
                    { id: "weightage", numeric: false, disablePadding: true, label: "Weightage" },
                  ]}
                />
              </div>

              <br></br>

              <div
                style={{
                  display: "flex",
                  flexDirection: "row-reverse",
                  marginLeft: "auto",
                }}
              >
                <BootButton
                  size="sm"
                  style={{
                    minWidth: "15%",
                    marginTop: "10px",
                    borderRadius: "3px",
                    paddingRight: "0.5em",
                    paddingLeft: "0.5em",
                    marginLeft: "0.5em",
                  }}
                  onClick={() => {
                    saveGlobalKPI(id, globalInsightName, rows);
                    setisVisible(!isVisible);
                  }}
                >
                  Save Global Insight
                </BootButton>
                <BootButton
                  variant="danger"
                  size="sm"
                  style={{
                    minWidth: "15%",
                    marginTop: "10px",
                    borderRadius: "3px",
                    paddingRight: "0.5em",
                    paddingLeft: "0.5em",
                  }}
                  onClick={() => deleteGlobalKPI(id, index)}
                >
                  Delete Global Insight
                </BootButton>
              </div>
            </div>
          </Card.Body>
        </Collapse>
      </Card>
    </div>
  );
}
