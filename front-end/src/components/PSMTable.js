import React, { useState, useRef } from "react";
import { Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TableHead,
  Paper,
  TextField,
  IconButton,
} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import CreateIcon from "@material-ui/icons/Create";
import uuid from "react-uuid";

const StyledTableCell = withStyles((theme) => ({
  head: {
    // backgroundColor: theme.palette.common.black,
    color: theme.palette.common.black,
  },
  body: {
    fontSize: 16,
  },
}))(TableCell);

const useStyles = makeStyles({
  table: {
    minWidth: "50%",
  },
});

export default function GlobalInsights({ kpiList, setKPIList }) {
  const weight = useRef(null);
  const [total, setTotal] = useState(100);
  const [showSubmit, setShowSubmit] = useState(false);
  const [flag, setFlag] = useState();
  const [saveBar, setSaveBar] = useState(false);
  const [delBar, setDelBar] = useState(false);

  function handleAddKPI() {
    var temp = { id: uuid(), index: kpiList.length + 1, kpi_name: "", kpi_type: "", weightage: 0 };
    setKPIList((prevKpis) => {
      return [...prevKpis, temp];
    });
  }

  function deleteKPI(id, index) {
    setFlag(index);
    var k,
      temp = kpiList;
    temp = temp.filter((obj) => obj.id !== id);
    for (k = 1; k <= temp.length; k++) {
      temp[k - 1].index = k;
    }
    setKPIList(temp);
    console.log("AFTER DELETE:", temp);
    setDelBar(true);
    temp.length === 0 ? setShowSubmit(false) : setShowSubmit(true);
  }

  function saveKPI(id, saved_filters, saved_groups, name, type, basis) {
    var j,
      temp1 = kpiList;
    for (j = 0; j < temp1.length; j++) {
      if (temp1[j].id === id) {
        temp1[j]["filters"] = saved_filters;
        temp1[j]["group_by"] = saved_groups;
        temp1[j]["kpi_name"] = name;
        temp1[j]["kpi_type"] = { type: type, basis: basis };
        setFlag(temp1[j]["index"]);
      }
    }
    setKPIList([...temp1]);
    console.log("AFTER SAVE:", temp1);
    setSaveBar(true);
    setShowSubmit(true);
  }

  const AddWeightage = (id) => {
    var w;
    kpiList.forEach((o) => {
      if (o.id === id) w = o.weightage;
    });
    console.log("W IDS", w, id);
    var temp1 = kpiList;
    var original = kpiList;
    var s = 0;
    temp1 = temp1.filter((o) => o.id !== id);
    temp1.forEach((d) => (s = s + d.weightage));
    if (s + w > total) {
      let extra = Math.abs(total - (s + w));
      original.forEach((d) => {
        if (d.id !== id) {
          d.weightage =
            d.weightage > 0 && d.weightage - extra / temp1.length > 0
              ? d.weightage - extra / temp1.length
              : 0;
        } else {
          d.weightage = w;
        }
      });
    } else {
      for (let j = 0; j < original.length; j++) {
        if (original[j].id === id) {
          original[j]["weightage"] = w;
        }
      }
    }

    console.log("SS", s);
    console.log("TOTAL", total);
    setKPIList([...original]);
  };
  const classes = useStyles();
  console.log("HELO FROM GLOBAL");
  console.log("KPI IN GLOBAL", kpiList);

  return (
    <div className="globalInsights card-body shadow">
      <h5 className="class-header w-100">PSM Scores</h5>
      <hr style={{ marginRight: "2%", width: "96%" }}></hr>
      <TableContainer
        component={Paper}
        style={{ width: "80%", marginLeft: "10%", marginTop: "2%" }}
      >
        <Table className={classes.table} size={"small"} aria-label="a dense table">
          <TableHead>
            <TableRow>
              <StyledTableCell>KPI Name</StyledTableCell>
              <StyledTableCell>KPI Type</StyledTableCell>
              <StyledTableCell>Weightage</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {kpiList.map((row) => (
              <TableRow key={row.id}>
                <StyledTableCell component="th" scope="row">
                  {row.kpi_name}
                </StyledTableCell>
                <StyledTableCell>{row.kpi_type.type ? row.kpi_type.type : ""}</StyledTableCell>
                <StyledTableCell>
                  <TextField
                    /* styles the label component */
                    InputLabelProps={{
                      style: {
                        height: 10,
                        // ...{ top: `0px` },
                        paddingTop: "-20px",
                        backgroundColor: "grey",
                      },
                    }}
                    /* styles the input component */
                    inputProps={{
                      style: {
                        height: 15,
                        marginTop: "-18px",
                        textAlign: "center",
                      },
                    }}
                    onBlur={() => {
                      AddWeightage(row.id);
                    }}
                    style={{ height: 20, width: 60 }}
                    id="standard-basic"
                    label=" "
                    size={"small"}
                    ref={weight}
                    value={row.weightage}
                    // onChange={(e) => (weight.current = parseInt(e.target.value))}
                    onChange={(e) => {
                      var temp3 = kpiList;
                      temp3.forEach((x) => {
                        if (x.id === row.id)
                          x.weightage = e.target.value > 0 ? parseInt(e.target.value) : 0;
                      });
                      setKPIList([...temp3]);
                    }}
                  />

                  {"%"}
                </StyledTableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
