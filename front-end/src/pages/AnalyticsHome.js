import React from "react";
import AppBar from "../components/AppBar";
import AnalyticsList from "../components/AnalyticsList";
import AnalyticsTable from "../components/AnalyticsTable";
import AnalyticsPallete from "../components/AnalyticsPallete";

function AnalyticsHome() {
  return (
    <div>
      <AppBar />
      {/* <AnalyticsList /> */}
      {/* <AnalyticsTable /> */}
      <AnalyticsPallete />
    </div>
  );
}

export default AnalyticsHome;
