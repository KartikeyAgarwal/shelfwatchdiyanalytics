import "date-fns";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  Button,
  Box,
  Collapse,
  IconButton,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Paper,
  DialogActions,
  DialogContentText,
  DialogTitle,
  DialogContent,
  Dialog,
} from "@material-ui/core";

import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import Draggable from "react-draggable";

function ThirdPage({ kpiList, open, handleClose }) {
  const useRowStyles = makeStyles({
    root: {
      "& > *": {
        borderBottom: "unset",
      },
    },
  });

  function Row(props) {
    const { row } = props;
    const [open, setOpen] = React.useState(false);
    const classes = useRowStyles();

    return (
      <React.Fragment>
        <TableRow className={classes.root}>
          <TableCell>
            <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </TableCell>
          <TableCell component="th" scope="row">
            {row.kpi_name}
          </TableCell>
          <TableCell>
            {row.kpi_type.type} {row.kpi_type.basis ? `[${row.kpi_type.basis}]` : ""}
          </TableCell>
          <TableCell>{row.weightage} %</TableCell>
        </TableRow>
        <TableRow>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0, flexDirection: "row" }} colSpan={3}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box margin={1}>
                <Typography variant="h6" gutterBottom component="div">
                  Filters
                </Typography>
                <Table size="small" aria-label="purchases">
                  <TableHead>
                    <TableRow>
                      <TableCell>Filter Type</TableCell>
                      <TableCell>Level One</TableCell>
                      <TableCell align="center">Level Two</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {row.filters.map((filterRow) => (
                      <TableRow key={filterRow.id}>
                        <TableCell component="th" scope="row">
                          {filterRow.filter_type}
                        </TableCell>
                        <TableCell>{filterRow.level_one ? filterRow.level_one : "--"}</TableCell>
                        <TableCell align="center">
                          {filterRow.level_two ? filterRow.level_two : "--"}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </TableCell>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0, flexDirection: "row" }} colSpan={2}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box margin={1}>
                <Typography variant="h6" gutterBottom component="div">
                  Group By
                </Typography>
                <Table size="small" aria-label="purchases">
                  <TableHead>
                    <TableRow>
                      <TableCell>Group</TableCell>
                      <TableCell>Level One</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {row.group_by.map((groupbyRow) => (
                      <TableRow key={groupbyRow.id}>
                        <TableCell component="th" scope="row">
                          {groupbyRow.group}
                        </TableCell>
                        <TableCell>{groupbyRow.level_one ? groupbyRow.level_one : "--"}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </React.Fragment>
    );
  }

  function PaperComponent(props) {
    return (
      <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
        <Paper {...props} />
      </Draggable>
    );
  }

  return (
    <div>
      <div
        className="card-body shadow"
        style={{
          width: "85%",
          marginLeft: "7%",
          marginRight: "7%",
          marginBottom: "1%",
          marginTop: "1%",
          backgroundColor: "",
        }}
      >
        <div className="row">
          <h5 className="class-header w-100" style={{ textAlign: "center", marginBottom: "1%" }}>
            Review and Submit
          </h5>
        </div>

        <div className="card-body border">
          <div className="row">
            <div
              style={{
                width: "100%",
                float: "right",
                marginRight: "5%",
                // borderRight: "1px solid grey",
              }}
            >
              <h5 style={{ textAlign: "center", margin: "2.5%" }}>Report Insights</h5>
              <TableContainer component={Paper} style={{ width: "100%" }}>
                <Table aria-label="collapsible table" style={{ width: "100%" }}>
                  <TableHead>
                    <TableRow>
                      <TableCell />
                      <TableCell>KPI Name</TableCell>
                      <TableCell>KPI Type</TableCell>
                      <TableCell>Weightage</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {kpiList.map((row) => (
                      <Row key={row.id} row={row} />
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
            {/* <hr width="1" size="5" style={{ display: "inline " }} /> */}
          </div>
        </div>

        <Dialog
          open={open}
          onClose={handleClose}
          PaperComponent={PaperComponent}
          aria-labelledby="draggable-dialog-title"
        >
          <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
            Confirm Submit
          </DialogTitle>
          <DialogContent>
            <DialogContentText>Are you sure you want to submit ?</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={handleClose} color="primary">
              Go Back
            </Button>
            <Button onClick={handleClose} color="primary">
              Submit
            </Button>
          </DialogActions>
        </Dialog>

        {/* <div className="card-body">
          <div className="row text-center">
            <BootsButton style={{ position: "relative", left: "40%", width: "20%" }}>
              Save and Proceed
            </BootsButton>
          </div>
        </div> */}
      </div>
    </div>
  );
}

export default ThirdPage;
