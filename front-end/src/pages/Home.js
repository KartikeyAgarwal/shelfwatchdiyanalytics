import React, { useState } from "react";

function Home() {
  return (
    <div>
      <h1 style={{ textAlign: "center" }}>HOME</h1>
    </div>
  );
}

export default Home;

// {
//   "report_insights": [
//     {
//       "id": "417e57-34f-c556-b31d-edd6ba2a480",
//       "index": 1,
//       "kpi_name": "KPI ONE",
//       "kpi_type": {
//         "type": "OSA",
//         "basis": ""
//       },
//       "weightage": 0,
//       "filters": [

//       ],
//       "group_by": [

//       ]
//     }
//   ],
//   "global_insights": [
//     {
//       "id": "612bef5-a8a-173-378a-a02720dfe66",
//       "index": 1,
//       "global_insight_name": "GTest",
//       "report_kpi_list": [
//         {
//           "id": "92dc2861-8a0c-4809-8eb4-dbebf490a679",
//           "report_kpi": "KPI ONE",
//           "aggregation_type": "Average",
//           "weightage": "200"
//         }
//       ]
//     }
//   ]

// analytics_name, projectName, tdVersionId, modifiedDate, projectId, tdVersionName, analyticsId
// }
