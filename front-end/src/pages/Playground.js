import "date-fns";
import React from "react";
import { TextField, FormControl, Select, MenuItem, Divider, Button } from "@material-ui/core/";
import { Button as BootsButton } from "react-bootstrap";
import { Alert, AlertTitle } from "@material-ui/lab";
import { makeStyles } from "@material-ui/core/styles";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers";
import IssuesModal from "../components/IssuesModal";
import ImagesModal from "../components/ImagesModal";

export default function Playground({ analyticsName, projectName }) {
  const [selectedDate, setSelectedDate] = React.useState(new Date("2014-08-18T21:11:54"));
  const [storeName, setStoreName] = React.useState("");
  const [categoryName, setCategoryName] = React.useState("");
  const [compatible, setCompatible] = React.useState(false);

  //MODAL CONFIG
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  //MODAL CONFIG END

  //IMAGE MODAL CONFIG
  const [openImage, setOpenImage] = React.useState(false);

  const handleClickOpenImage = () => {
    setOpenImage(true);
  };
  const handleCloseImage = () => {
    setOpenImage(false);
  };
  // IMAGE MODAL END

  const handleStoreChange = (e) => {
    setStoreName(e.target.value);
  };
  const handleCategoryChange = (e) => {
    setCategoryName(e.target.value);
  };

  const useStyles = makeStyles((theme) => ({
    formControl: {
      // margin: theme.spacing(1),
      minWidth: 120,
      marginLeft: "5%",
      marginTop: "-2%",
    },
  }));

  const classes = useStyles();

  return (
    <div>
      {/* <div className="header">
        <span style={{ lineHeight: "2.5em" }}>ShelfWatch DIY Analytics</span>
      </div> */}

      <div
        className="card-body shadow"
        style={{
          width: "85%",
          marginLeft: "7%",
          marginRight: "7%",
          marginBottom: "1%",
          marginTop: "1%",
          backgroundColor: "",
        }}
      >
        <div className="row">
          <h5 className="class-header w-100" style={{ textAlign: "center", marginBottom: "1%" }}>
            Playground
          </h5>
        </div>

        <div className="card-body border">
          <div className="row">
            <div
              style={{
                width: "45%",
                float: "left",
                marginLeft: "5%",
                borderRight: "1px solid grey",
              }}
            >
              <h6 style={{ textAlign: "center", marginBottom: "5%" }}> Analytics Data</h6>
              <p style={{ marginTop: "5%" }}>
                Analytics Name : &nbsp;&nbsp;&nbsp;&nbsp;{" "}
                <TextField
                  id="outlined-basic"
                  size="small"
                  label=""
                  variant="outlined"
                  value={analyticsName}
                  InputProps={{
                    readOnly: true,
                  }}
                />
              </p>

              <p>
                Analytics Details : &nbsp;&nbsp;&nbsp;{" "}
                <TextField
                  id="outlined-basic"
                  size="small"
                  label="Insights Available"
                  variant="outlined"
                />
              </p>
            </div>
            {/* <hr width="1" size="5" style={{ display: "inline " }} /> */}
            <div
              style={{
                width: "45%",
                float: "right",
                marginLeft: "5%",
              }}
            >
              <h6 style={{ textAlign: "center", marginBottom: "5%" }}> Select Source Data</h6>
              <div style={{ marginTop: "1.5%" }}>
                Project Name : &nbsp;
                <TextField
                  id="outlined-basic"
                  size="small"
                  label=""
                  variant="outlined"
                  value={projectName}
                  InputProps={{
                    readOnly: true,
                  }}
                />
              </div>
              <div style={{ marginTop: "5%" }}>
                Select Date :
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    style={{ marginLeft: "5%" }}
                    id="date-picker-dialog"
                    label="Date picker dialog"
                    format="mm/dd/yyyy"
                    value={selectedDate}
                    onChange={(date) => setSelectedDate(date)}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                  />
                </MuiPickersUtilsProvider>
              </div>
              <Divider style={{ backgroundColor: "black", width: "90%", marginTop: "10%" }} />
              <div className="row">
                <div style={{ marginTop: "10%" }}>
                  Store Name :
                  <FormControl
                    style={{ display: null }}
                    className={classes.formControl}
                    size="medium"
                  >
                    {/* <InputLabel id="demo-simple-select-label">Store Name</InputLabel> */}
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={storeName}
                      onChange={handleStoreChange}
                    >
                      <MenuItem value={"Store ABC"}>Store ABC</MenuItem>
                      <MenuItem value={"Store BCD"}>Store BCD</MenuItem>
                      <MenuItem value={"Store XYZ"}>Store XYZ</MenuItem>
                    </Select>
                  </FormControl>
                </div>
                <div style={{ marginTop: "5%" }}>
                  Category :
                  <FormControl
                    style={{ display: null }}
                    className={classes.formControl}
                    size="medium"
                  >
                    {/* <InputLabel id="demo-simple-select-label">Category</InputLabel> */}
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={categoryName}
                      onChange={handleCategoryChange}
                    >
                      <MenuItem value={"Shampoo"}>Shampoo</MenuItem>
                      <MenuItem value={"Shampoo"}>Shampoo</MenuItem>
                      <MenuItem value={"Shampoo"}>Shampoo</MenuItem>
                    </Select>
                  </FormControl>
                </div>
              </div>

              <BootsButton style={{ float: "right", marginRight: "10%" }}>Run</BootsButton>
            </div>
          </div>
        </div>

        <div className="card-body border">
          <div className="row">
            <div style={{ float: "left", marginLeft: "5%" }}>
              <br></br>
              <h5 style={{ textAlign: "left", marginBottom: "3%" }}> Store data</h5>

              <p style={{ marginTop: "-2%" }}>
                <b>
                  <span> Batches : 2</span>
                  <span style={{ marginLeft: "5%" }}> Completed Images : 10</span>
                  <span style={{ marginLeft: "5%" }}> Incomplete Images : 0</span>
                  <span style={{ marginLeft: "5%" }}> Invalid Images : 1</span>
                </b>
                <Button
                  variant="outlined"
                  style={{ marginLeft: "5%" }}
                  onClick={handleClickOpenImage}
                >
                  View Images
                </Button>
              </p>
            </div>
          </div>
        </div>

        <div className="card-body border">
          <div className="row">
            <div style={{ float: "left", marginLeft: "4.5%" }}>
              <h5 style={{ textAlign: "left", marginBottom: "3%" }}> Analytics</h5>
              {compatible ? (
                <div style={{ marginBottom: "2%" }}>
                  <Alert
                    onClose={() => setCompatible(false)}
                    size="small"
                    severity="success"
                    style={{
                      marginRight: 10,
                      width: "30%",
                    }}
                  >
                    <AlertTitle>Success!</AlertTitle>
                    KPIs Compatible with the stored data.
                  </Alert>
                </div>
              ) : (
                <div className="row" style={{ marginBottom: "2%" }}>
                  <Alert
                    onClose={() => setCompatible(true)}
                    size="small"
                    severity="warning"
                    style={{
                      marginRight: 10,
                      width: "32%",
                    }}
                  >
                    <AlertTitle>Error!</AlertTitle>
                    Not compatible with the stored data — <strong>KPI B</strong>
                  </Alert>
                  <Button
                    style={{ width: "fit-content", height: "fit-content", alignSelf: "center" }}
                    variant="text"
                    color="primary"
                    onClick={handleClickOpen}
                  >
                    See Issues Found
                  </Button>
                </div>
              )}
            </div>
          </div>
        </div>

        <div className="card-body border">
          <div className="row">
            <div
              className="reportInsigh"
              style={{
                float: "left",
                width: "45.5%",
                marginTop: "0.5%",
                marginBottom: "2%",
                marginLeft: "3%",
                marginRight: "1.5%",
                backgroundColor: "#F8F8F8",
              }}
            >
              <h5
                className="class-header w-100"
                style={{ padding: "0.5em", marginBottom: "0.7rem" }}
              >
                Report Insights
              </h5>
              <hr style={{ marginLeft: "2%", marginRight: "2%", width: "96%" }}></hr>
            </div>

            <div
              className="globalInsigh"
              style={{
                float: "left",
                width: "45.5%",
                marginTop: "0.5%",
                marginBottom: "2%",
                marginLeft: "3%",
                marginRight: "1.5%",
                backgroundColor: "#F8F8F8",
              }}
            >
              <h5
                className="class-header w-100"
                style={{ padding: "0.5em", marginBottom: "0.7rem" }}
              >
                Global Insights
              </h5>
              <hr style={{ marginLeft: "2%", marginRight: "2%", width: "96%" }}></hr>
            </div>
          </div>
        </div>

        <div className="card-body">
          <div className="row text-center">
            <BootsButton style={{ position: "relative", left: "40%", width: "20%" }}>
              Save and Proceed
            </BootsButton>
          </div>
        </div>
      </div>
      <IssuesModal
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
        open={open}
        setOpen={setOpen}
      />
      <ImagesModal
        handleClickOpen={handleClickOpenImage}
        handleClose={handleCloseImage}
        open={openImage}
        setOpen={setOpenImage}
      />
    </div>
  );
}

//BACKDROP
