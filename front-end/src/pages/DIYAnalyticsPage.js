import React, { useState, useRef, useEffect } from "react";
// import uuid from 'react-uuid'
import "../App.css";
// import "bootstrap/dist/css/bootstrap.min.css";
import ReportInsights from "../components/report_insights/Report_Insights";
import GlobalInsights from "../components/global_insights/Global_Insights";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import PSMTable from "../components/PSMTable";

const LOCAL_STORAGE_KEY = "react-app";

function DIYAnalyticsPage({
  analyticsName,
  setAnalyticsName,
  projectName,
  setProjectName,
  kpiList,
  setKPIList,
  kpiGlobalList,
  setKPIGlobalList,
  finalData,
  setFinalData,
}) {
  //useEffect to store the items data in local storage
  // useEffect(() => {
  //   const kpiList = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
  //   if (kpiList) setKPIList(kpiList);
  // }, []);

  // useEffect(() => {
  //   localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(kpiList));
  // }, [kpiList]);

  const useStyles = makeStyles((theme) => ({
    root: {
      "& > *": {
        // margin: theme.spacing(1),
        // width: "25ch",
      },
    },
  }));

  const classes = useStyles();

  return (
    <div>
      <div className="appDiv">
        <form
          className={classes.root}
          style={{ margin: "2%", marginLeft: "-1%" }}
          noValidate
          autoComplete="off"
        >
          <span style={{ marginLeft: "5%", marginRight: "1%" }}>Analytics Name : </span>
          <TextField
            id="outlined-basic"
            value={analyticsName}
            onChange={(e) => setAnalyticsName(e.target.value)}
            label=""
            size="small"
            variant="outlined"
          />
          <span style={{ marginLeft: "5%", marginRight: "1%" }}>Project Name : </span>
          <TextField
            id="outlined-basic"
            value={projectName}
            onChange={(e) => setProjectName(e.target.value)}
            label=""
            size="small"
            variant="outlined"
          />
        </form>

        <ReportInsights
          kpiList={kpiList}
          setKPIList={setKPIList}
          finalData={finalData}
          setFinalData={setFinalData}
        />
        <GlobalInsights
          kpiGlobalList={kpiGlobalList}
          setKPIGlobalList={setKPIGlobalList}
          kpiList={kpiList}
          finalData={finalData}
          setFinalData={setFinalData}
        />
        <PSMTable kpiList={kpiList} />
      </div>
    </div>
  );
}

export default DIYAnalyticsPage;
